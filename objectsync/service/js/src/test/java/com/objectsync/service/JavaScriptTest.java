package com.objectsync.service;
//
//import java.net.InetSocketAddress;
//import java.net.MalformedURLException;
//import java.util.concurrent.Executors;
//
//import org.eclipse.jetty.server.Handler;
//import org.eclipse.jetty.server.ServerConnector;
//import org.eclipse.jetty.server.handler.ContextHandler;
//import org.eclipse.jetty.server.handler.ContextHandlerCollection;
//import org.eclipse.jetty.server.handler.ResourceHandler;
//import org.jboss.netty.bootstrap.ServerBootstrap;
//import org.jboss.netty.channel.Channel;
//import org.jboss.netty.channel.ChannelPipeline;
//import org.jboss.netty.channel.ChannelPipelineFactory;
//import org.jboss.netty.channel.Channels;
//import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
//import org.jboss.netty.handler.codec.http.HttpRequestDecoder;
//import org.jboss.netty.handler.codec.http.HttpResponseEncoder;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.objectfabric.JVMServer;
//import org.objectfabric.Memory;
//import org.objectfabric.NettySession;
//import org.objectfabric.Server;
//
//import com.gargoylesoftware.htmlunit.WebClient;
//import com.gargoylesoftware.htmlunit.html.HtmlPage;
//
//public class JavaScriptTest {
//
//	private ServerBootstrap bootstrap;
//	private Channel channel;
//	private org.eclipse.jetty.server.Server jetty;
//
//	static Handler createResourceContext(String contextPath, String baseDir)
//			throws MalformedURLException {
//		ContextHandler context = new ContextHandler("/" + contextPath);
//		ResourceHandler resource = new ResourceHandler();
//		resource.setResourceBase(baseDir);
//		resource.setDirectoriesListed(true);
//		context.setHandler(resource);
//		return context;
//	}
//
//	@Before
//	public void before() throws Exception {
//		Memory store = new Memory(false);
//		final Server server = new JVMServer();
//		server.addURIHandler(store);
//
//		/*
//		 * Start a WebSocket server. (C.f. https://netty.io)
//		 */
//		bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory( //
//				Executors.newCachedThreadPool(),
//				Executors.newCachedThreadPool()));
//		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
//			@Override
//			public ChannelPipeline getPipeline() throws Exception {
//				ChannelPipeline pipeline = Channels.pipeline();
//				pipeline.addLast("decoder", new HttpRequestDecoder());
//				pipeline.addLast("encoder", new HttpResponseEncoder());
//				pipeline.addLast("objectsync", new NettySession(server));
//				return pipeline;
//			}
//		});
//
//		channel = bootstrap.bind(new InetSocketAddress(9191));
//
//		// start jetty server
//		jetty = new org.eclipse.jetty.server.Server();
//		ServerConnector connector = new ServerConnector(jetty);
//		connector.setPort(9292);
//		jetty.addConnector(connector);
//
//		String staticDir = System.getProperty("test.resource.static") == null ? "target/objectsync-service-js-0.9.2-SNAPSHOT"
//				: System.getProperty("test.resource.static");
//		String testDir = System.getProperty("test.resource.test") == null ? "target/test-classes"
//				: System.getProperty("test.resource.test");
//
//		ContextHandlerCollection handlers = new ContextHandlerCollection();
//		handlers.addHandler(createResourceContext("base", staticDir));
//		handlers.addHandler(createResourceContext("test", testDir));
//		// handlers.addHandler(new DefaultHandler());
//		jetty.setHandler(handlers);
//		jetty.start();
//	}
//
//	@Test
//	public void test() throws Exception {
//		WebClient client = new WebClient();
//		client.waitForBackgroundJavaScriptStartingBefore(2000);
//		HtmlPage page = client.getPage("http://localhost:9292/test/test.html");
//		Object object = page.executeJavaScript(
//				"new OS.ObjectSync('localhost:9191');").getJavaScriptResult();
//		System.out.println(object);
//		Assert.assertEquals(true, true);
//	}
//
//	@After
//	public void after() throws Exception {
//		channel.unbind().await();
//		jetty.stop();
//	}
//}
