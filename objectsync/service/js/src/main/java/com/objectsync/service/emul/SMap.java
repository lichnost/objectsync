package com.objectsync.service;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import jsinterop.annotations.JsFunction;
import jsinterop.annotations.JsIgnore;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import net.sourceforge.htmlunit.corejs.javascript.annotations.JSFunction;

import org.objectfabric.KeyListener;
import org.objectfabric.Resource;
import org.objectfabric.TMap;

import com.objectsync.service.SArray.ArrayListener;

@JsType(name = "SMap", namespace = "OS")
public final class SMap<K, V> extends SBase implements Map<K, V> {

	private TMap<K, V> _map;

	private Map<MapListener<K>, KeyListener<K>> _listeners = new ConcurrentHashMap<SMap.MapListener<K>, KeyListener<K>>();

	public <B extends SBase> SMap(B base) {
		this(base.getResource(), new TMap<K, V>(base.getResource()));
	}

	SMap(Resource resource, TMap<K, V> map) {
		super(resource, map);
		_map = map;
	}

	public int size() {
		return _map.size();
	}

	public boolean isEmpty() {
		return _map.isEmpty();
	}

	public boolean containsKey(Object key) {
		return _map.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return _map.containsValue(value);
	}

	public V get(Object key) {
		return _map.get(key);
	}

	public V put(K key, V value) {
		return _map.put(key, value);
	}

	public V remove(Object key) {
		return _map.remove(key);
	}

	@JsIgnore
	public void putAll(Map<? extends K, ? extends V> m) {
		_map.putAll(m);
	}

	public void clear() {
		_map.clear();
	}

	@JsIgnore
	public Set<K> keySet() {
		return _map.keySet();
	}

	@JsIgnore
	public Collection<V> values() {
		return _map.values();
	}

	@JsIgnore
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return _map.entrySet();
	}

	public void addListener(final MapListenerConfig config) {
		KeyListener<K> mapListener = new KeyListener<K>() {

			public void onPut(K key) {
				listener.onPut(key);
			}

			public void onRemove(K key) {
				listener.onRemove(key);
			}

			public void onClear() {
				listener.onClear();
			}

		};
		_map.addListener(mapListener);
		_listeners.put(listener, mapListener);
	}


	@JsType(isNative = true)
	public static class MapListenerConfig {

		@JsProperty
		MapPutListener<K> onput;
		
		@JsProperty
		MapRemoveListener<K> onremove;
		
		@JsProperty
		MapClearListener<K> onclear;
		
	}

	@JsFunction
	public interface MapPutListener<K> {

		public void onPut(K key);

	}

	@JsFunction
	public interface MapRemoveListener<K> {

		public void onRemove(K key);

	}

	@JsFunction
	public interface MapClearListener<K> {

		public void onClear();

	}

}
