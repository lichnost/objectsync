package com.objectsync.service.emul;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.objectfabric.IndexListener;
import org.objectfabric.Resource;

public final class SObject<E> extends SBase {

	private TRef _object;

	private Map<ObjectListener, IndexListener> _listeners = new ConcurrentHashMap<SObject.ObjectListener, IndexListener>();

	public SObject(SBase base) {
		this(base.getResource(), new TRef(base.getResource()));
	}

	SObject(Resource resource, TRef object) {
		super(resource, object);
		_object = object;
	}

	@SuppressWarnings("unchecked")
	public E get() {
		return (E) _object.object();
	}

	public void set(E value) {
		_object.object(value);
	}

	public void addListener(final ObjectListener listener) {
		IndexListener objectListener = new IndexListener() {

			public void onSet(int index) {
				if (index == TRef.OBJECT_INDEX) {
					listener.onSet();
				}
			}
		};
		_object.addListener(objectListener);
		_listeners.put(listener, objectListener);
	}

	public void removeListener(ObjectListener listener) {
		_object.removeListener(_listeners.remove(listener));
	}

	public interface ObjectListener {

		public void onSet();

	}

}
