package com.objectsync.service.emul;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.objectfabric.Resource;
import org.objectfabric.ResourceListener;
import org.objectfabric.TArray;
import org.objectfabric.TMap;
import org.objectfabric.TObject;
import org.objectfabric.TSet;

public final class SRoot extends SBase {

	private Map<RootListener, ResourceListener> _listeners = new ConcurrentHashMap<RootListener, ResourceListener>();

	SRoot(Resource resource) {
		super(resource, resource);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public SBase get() {
		TObject object = (TObject) getResource().get();
		System.out.println("Object: " + object);
		SBase result = null;
		if (object instanceof TRef) {
			result = new SObject(getResource(), (TRef) object);
		} else if (object instanceof TArray) {
			result = new SArray(getResource(), (TArray) object);
		} else if (object instanceof TRef) {
			result = new SMap(getResource(), (TMap) object);
		} else if (object instanceof TRef) {
			result = new SSet(getResource(), (TSet) object);
		}
		return result;
	}

	public void set(SBase object) {
		getResource().set(object.getBase());
	}

	@Override
	public boolean equals(Object obj) {
		return getResource().equals(obj);
	}

	@Override
	public int hashCode() {
		return getResource().hashCode();
	}

	@Override
	public String toString() {
		return getResource().toString();
	}

	public void addListener(final RootListener listener) {
		ResourceListener resourceListener = new ResourceListener() {

			public void onSet() {
				listener.onSet();
			}

			public void onDelete() {
				listener.onDelete();
			}
		};
		getResource().addListener(resourceListener);
		_listeners.put(listener, resourceListener);
	}

	public void removeListener(RootListener listener) {
		getResource().removeListener(_listeners.remove(listener));
	}

	public interface RootListener {

		public void onSet();

		public void onDelete();

	}

}
