package com.objectsync.service;

import com.google.gwt.core.client.JavaScriptObject;

public class JSObjectListener extends JavaScriptObject {

	native JavaScriptObject getOnSet() /*-{ return this.onset; }-*/;
	
	native JavaScriptObject getOnDelete() /*-{ return this.ondelete; }-*/;
	
}
