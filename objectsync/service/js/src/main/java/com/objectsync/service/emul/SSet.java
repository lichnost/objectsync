
package com.objectsync.service.emul;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.objectfabric.KeyListener;
import org.objectfabric.Resource;
import org.objectfabric.TSet;

public final class SSet<E> extends SBase implements Set<E> {

    private TSet<E> _set;

    private Map<SetListener<E>, KeyListener<E>> _listeners = new ConcurrentHashMap<SetListener<E>, KeyListener<E>>();
    
    public <B extends SBase> SSet(B base) {
        this(base.getResource(), new TSet<E>(base.getResource()));
    }
    
    SSet(Resource resource, TSet<E> set) {
        super(resource, set);
        _set = set;
    }

    public int size() {
        return _set.size();
    }

    public boolean isEmpty() {
        return _set.isEmpty();
    }

    public boolean contains(Object o) {
        return _set.contains(o);
    }

    public Iterator<E> iterator() {
        return _set.iterator();
    }

    public Object[] toArray() {
        return _set.toArray();
    }

    public <T> T[] toArray(T[] a) {
        return _set.toArray(a);
    }

    public boolean add(E e) {
        return _set.add(e);
    }

    public boolean remove(Object o) {
        return _set.remove(o);
    }

    public boolean containsAll(Collection<?> c) {
        return _set.containsAll(c);
    }

    public boolean addAll(Collection<? extends E> c) {
        return _set.addAll(c);
    }

    public boolean retainAll(Collection<?> c) {
        return _set.retainAll(c);
    }

    public boolean removeAll(Collection<?> c) {
        return _set.removeAll(c);
    }

    public void clear() {
        _set.clear();
    }

    public void addListener(final SetListener<E> listener) {
        KeyListener<E> resourceListener = new KeyListener<E>() {

            public void onPut(E key) {
                listener.onPut(key);
            }

            public void onRemove(E key) {
                listener.onRemove(key);
            }

            public void onClear() {
                listener.onClear();
            }

        };
        _set.addListener(resourceListener);
        _listeners.put(listener, resourceListener);
    }

    public void removeListener(SetListener<E> listener) {
        _set.removeListener(_listeners.remove(listener));
    }

    public interface SetListener<E> {

        public void onPut(E key);

        public void onRemove(E key);

        public void onClear();
    }

}
