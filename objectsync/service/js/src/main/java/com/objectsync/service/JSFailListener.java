package com.objectsync.service;

import jsinterop.annotations.JsFunction;

@JsFunction
public interface JSFailListener {

	public void failed(String message);
	
}
