package com.objectsync.service;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import com.google.gwt.core.client.JavaScriptObject;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class JSObjectSyncConfig extends JavaScriptObject {

	@JsProperty
	public String address;

	@JsProperty
	public boolean ssl;

}
