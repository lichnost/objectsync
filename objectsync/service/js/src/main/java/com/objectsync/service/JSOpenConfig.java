package com.objectsync.service;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class JSOpenConfig {

	@JsProperty
	JSObjectSync.JSOpenListener onopen;

	@JsProperty
	JSFailListener onfail;

}
