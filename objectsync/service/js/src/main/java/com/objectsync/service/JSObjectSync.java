package com.objectsync.service;

import jsinterop.annotations.JsFunction;
import jsinterop.annotations.JsType;

import org.objectfabric.GWTWorkspace;

@JsType(name = "ObjectSync")
public final class JSObjectSync extends ObjectSync {

	public JSObjectSync(JSObjectSyncConfig config) {
		super(new GWTWorkspace(), config.address, config.ssl);
	}

	public void open(String key, final JSOpenConfig config) {
		super.open(key, new AsyncCallback<SRoot>() {

			@Override
			public void onSuccess(SRoot result) {
				if (config.onopen != null) {
					config.onopen.opened(result);
				}
			}

			@Override
			public void onFailure(Exception e) {
				if (config.onfail != null) {
					config.onfail.failed(e.getMessage());
				}
			}
		});
	}

	@JsFunction
	public interface JSOpenListener {

		public void opened(SRoot root);

	}

}
