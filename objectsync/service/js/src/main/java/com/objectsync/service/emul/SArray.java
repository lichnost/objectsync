package com.objectsync.service;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import jsinterop.annotations.JsIgnore;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import net.sourceforge.htmlunit.corejs.javascript.annotations.JSFunction;

import org.objectfabric.IndexListener;
import org.objectfabric.Resource;
import org.objectfabric.TArray;

@JsType(name = "SArray", namespace = "OS")
public final class SArray<E> extends SBase implements Iterable<E> {

	private TArray<E> _array;

	private Map<ArrayListener, IndexListener> _listeners = new ConcurrentHashMap<SArray.ArrayListener, IndexListener>();

	public <B extends SBase> SArray(B base, int length) {
		this(base.getResource(), new TArray<E>(base.getResource(), length));
	}

	SArray(Resource resource, TArray<E> array) {
		super(resource, array);
		_array = array;
	}

	public void set(int index, E value) {
		_array.set(index, value);
	}

	public E get(int index) {
		return _array.get(index);
	}

	public int length() {
		return _array.length();
	}

	@JsIgnore
	public Iterator<E> iterator() {
		return _array.iterator();
	}

	public void addListener(final ArrayListenerConfig config) {
		IndexListener resourceListener = new IndexListener() {

			public void onSet(int index) {
				if (config.onset != null) {
					config.onset.onSet(index);
				}
			}

		};
		_array.addListener(resourceListener);
		_listeners.put(listener, resourceListener);
	}

	@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
	public static class ArrayListenerConfig {

		@JsProperty
		ArrayListener onset;

	}

	@JSFunction
	public interface ArrayListener {

		public void onSet(int index);

	}

}
