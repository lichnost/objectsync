
package com.objectsync.service;

import java.net.InetSocketAddress;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.codec.http.HttpRequestDecoder;
import org.jboss.netty.handler.codec.http.HttpResponseEncoder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectfabric.JVMServer;
import org.objectfabric.Memory;
import org.objectfabric.NettySession;
import org.objectfabric.Server;

import com.objectsync.service.SObject.ObjectListener;
import com.objectsync.service.SRoot.RootListener;

public class JVMObjectSyncTests {

    private ServerBootstrap bootstrap;

    private Channel channel;

    @Before
    public void start() {
        ObjectSyncObjectModel.register();

        Memory store = new Memory(false);
        final Server server = new JVMServer();
        server.addURIHandler(store);

        bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory( //
                Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
        bootstrap.setPipelineFactory(new ChannelPipelineFactory() {

            @Override
            public ChannelPipeline getPipeline() throws Exception {
                ChannelPipeline pipeline = Channels.pipeline();
                pipeline.addLast("decoder", new HttpRequestDecoder());
                pipeline.addLast("encoder", new HttpResponseEncoder());
                pipeline.addLast("objectsync", new NettySession(server));
                return pipeline;
            }
        });

        channel = bootstrap.bind(new InetSocketAddress(8080));

    }

    @Test
    public void test() throws Exception {
        // test for local set then get
        JVMObjectSync os = new JVMObjectSync("localhost:8080");
        SRoot root = os.open("test");
        SObject<String> object = new SObject<String>(root);
        root.set(object);
        object.set("Test");
        Assert.assertEquals("Test", ((SObject) root.get()).get());
        os.close();

        // test for asynchronous get/set
        os = new JVMObjectSync("localhost:8080");

        final CountDownLatch latch = new CountDownLatch(1);
        root = os.open("test");
        root.addListener(new RootListener() {

            @Override
            public void onSet() {
                latch.countDown();
            }

            @Override
            public void onDelete() {
            }
        });
        if (root.get() == null) {
            latch.await();
        }

        final SObject<String> objectAsync = (SObject<String>) root.get();
        Assert.assertEquals("Test", objectAsync.get());

        final CountDownLatch objectLatch = new CountDownLatch(1);
        objectAsync.addListener(new ObjectListener() {

            @Override
            public void onSet() {
                Assert.assertEquals("TestNew", objectAsync.get());
                objectLatch.countDown();
            }

        });
        objectAsync.set("TestNew");
        objectLatch.await();

        Assert.assertEquals("TestNew", objectAsync.get());

        os.close();
    }

    @After
    public void stop() throws Exception {
        channel.unbind().await();
    }

}
