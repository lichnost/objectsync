package com.objectsync.service;

import org.objectfabric.Address;
import org.objectfabric.Headers;
import org.objectfabric.JVMWorkspace;
import org.objectfabric.Netty;

/**
 * JVM implementation of {@link ObjectSync}
 * 
 * See {@link ObjectSync} for more details.
 * 
 * @author semenov_pa
 *
 */
public final class JVMObjectSync extends ObjectSync {

	public JVMObjectSync(String address) {
		this(address, false);
	}

	public JVMObjectSync(String address, boolean ssl) {
		super(new JVMWorkspace(), address, ssl);
		_workspace.addURIHandler(new Netty() {

			@Override
			protected Headers getHeaders(Address address) {
				return JVMObjectSync.this.getHeaders(address);
			}
			
		});
	}

}
