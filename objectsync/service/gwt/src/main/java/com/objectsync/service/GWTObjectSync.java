package com.objectsync.service;

import org.objectfabric.GWTWorkspace;

/**
 * GWT implementation of {@link ObjectSync}
 * 
 * See {@link ObjectSync} for more details.
 * 
 * @author semenov_pa
 *
 */
public final class GWTObjectSync extends ObjectSync {

	public GWTObjectSync(String address) {
		this(address, false);
	}

	public GWTObjectSync(String address, boolean ssl) {
		super(new GWTWorkspace(), address, ssl);
	}

}
