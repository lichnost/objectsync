package com.objectsync.service;

import org.jboss.netty.channel.socket.ClientSocketChannelFactory;
import org.jboss.netty.channel.socket.oio.OioClientSocketChannelFactory;

import android.view.View;

import org.objectfabric.Address;
import org.objectfabric.AndroidWorkspace;
import org.objectfabric.Headers;
import org.objectfabric.Netty;

/**
 * Android implementation of {@link ObjectSync}
 * 
 * See {@link ObjectSync} for more details.
 * 
 * @author semenov_pa
 *
 */
public final class AndroidObjectSync extends ObjectSync {

	public AndroidObjectSync(View view, String address) {
		this(view, address, false);
	}

	public AndroidObjectSync(final View view, String address, boolean ssl) {
		super(new AndroidWorkspace(view), address, ssl);
		// Executor executor = new Executor() {
		//
		// @Override
		// public void execute(Runnable command) {
		// view.post(command);
		// }
		//
		// };
		ClientSocketChannelFactory factory = new OioClientSocketChannelFactory();
		Netty netty = new Netty(factory) {

			@Override
			protected Headers getHeaders(Address address) {
				return AndroidObjectSync.this.getHeaders(address);
			}

		};
		_workspace.addURIHandler(netty);
	}

}
