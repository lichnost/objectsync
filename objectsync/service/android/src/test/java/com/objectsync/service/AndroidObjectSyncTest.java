package com.objectsync.service;
//
//import java.net.InetSocketAddress;
//import java.util.concurrent.CountDownLatch;
//import java.util.concurrent.Executors;
//
//import org.jboss.netty.bootstrap.ServerBootstrap;
//import org.jboss.netty.channel.ChannelPipeline;
//import org.jboss.netty.channel.ChannelPipelineFactory;
//import org.jboss.netty.channel.Channels;
//import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
//import org.jboss.netty.handler.codec.http.HttpRequestDecoder;
//import org.jboss.netty.handler.codec.http.HttpResponseEncoder;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.robolectric.Robolectric;
//import org.robolectric.RobolectricTestRunner;
//import org.robolectric.annotation.Config;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.view.View;
//import android.view.ViewStub;
//
//import com.objectsync.JVMServer;
//import com.objectsync.Memory;
//import com.objectsync.NettySession;
//import com.objectsync.Server;
//import com.objectsync.service.SObject.ObjectListener;
//
//@RunWith(RobolectricTestRunner.class)
//@Config(manifest = Config.NONE)
//public class AndroidObjectSyncTest {
//
//	private TestActivity activity;
//	private ServerBootstrap bootstrap;
//
//	@Before
//	public void start() {
//		activity = Robolectric.setupActivity(TestActivity.class);
//
//		Memory store = new Memory(false);
//		final Server server = new JVMServer();
//		server.addURIHandler(store);
//
//		bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory( //
//				Executors.newCachedThreadPool(),
//				Executors.newCachedThreadPool()));
//		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
//
//			@Override
//			public ChannelPipeline getPipeline() throws Exception {
//				ChannelPipeline pipeline = Channels.pipeline();
//				pipeline.addLast("decoder", new HttpRequestDecoder());
//				pipeline.addLast("encoder", new HttpResponseEncoder());
//				pipeline.addLast("objectsync", new NettySession(server));
//				return pipeline;
//			}
//		});
//
//		bootstrap.bind(new InetSocketAddress(8080));
//
//	}
//
//	@Test
//	public void test() throws Exception {
//		// test for local set then get
//		ObjectSync os = new ObjectSync(activity.view, "localhost:8080");
//		SObject<String> object = os.createObject("test");
//		object.set("Test");
//		Assert.assertEquals("Test", object.get());
//		os.close();
//
//		// test for asynchronous get/set
//		os = new ObjectSync(activity.view, "localhost:8080");
//		final SObject<String> objectAsync = os.getObject("test");
//
//		final CountDownLatch latch = new CountDownLatch(1);
//		objectAsync.addListener(new ObjectListener() {
//
//			@Override
//			public void onSet() {
//				Assert.assertEquals("TestNew", objectAsync.get());
//				latch.countDown();
//			}
//
//			@Override
//			public void onDelete() {
//			}
//		});
//		objectAsync.set("TestNew");
//		latch.await();
//
//		Assert.assertEquals("TestNew", objectAsync.get());
//
//		os.close();
//	}
//
//	@After
//	public void stop() {
//		bootstrap.shutdown();
//	}
//
//	class TestActivity extends Activity {
//
//		View view;
//
//		@Override
//		protected void onCreate(Bundle savedInstanceState) {
//			view = new ViewStub(this);
//			setContentView(view);
//		}
//
//	}
//
//}
