
package com.objectsync.service;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.objectfabric.KeyListener;
import org.objectfabric.Resource;
import org.objectfabric.TSet;
import org.objectfabric.Workspace;

import com.objectsync.service.SObject.ObjectListener;

/**
 * Transactional set. For each thread this class behaves like an HashSet, except:<br>
 * <br>
 * - It does not support null elements. <br>
 * - It does not implement clone(). <br>
 * <br>
 * Iterators never throw {@link ConcurrentModificationException}, but currently can offer
 * an inconsistent view of the collection. Items can be missed or provided twice if the
 * collection is updated concurrently. This can be avoided by wrapping the iteration in a
 * {@link ObjectSync#atomicRead(Runnable)} block, which offers a consistent snapshot of the
 * collection.<br>
 * <br>
 * Some methods on the Set interface both read and write values. E.g. method
 * {@link SSet#add(E)} returns false if the element was present. In the context of a
 * transaction, returning a value induces a read. This read will be checked for conflicts
 * at commit time, and might invalidate the transaction, e.g. if the item has been added
 * or removed concurrently. To avoid this read, transactional collections provide twin
 * methods which do not return values, e.g. {@link TSet#addOnly(E)} which returns void.<br>
 * <br>
 * Keys implementation notes:<br>
 * <br>
 * If a key's hashCode() or equals(Object) method throws an exception, there are two cases
 * where the exception will be caught and logged by ObjectFabric. If the transaction
 * inserting the entry to the map is currently committing, and if it is already committed.
 * In the first case the transaction will be aborted. In the second the entry will be
 * removed from snapshots of the map seen by transactions started after the exception has
 * been thrown.
 */
public final class SSet<E> extends SBase implements Set<E> {

    private TSet<E> _set;

    private Map<SetListener<E>, KeyListener<E>> _listeners = new ConcurrentHashMap<SetListener<E>, KeyListener<E>>();
    
    public <B extends SBase> SSet(B base) {
        this(base.getResource(), new TSet<E>(base.getResource()));
    }
    
    SSet(Resource resource, TSet<E> set) {
        super(resource, set);
        _set = set;
    }

    public int size() {
        return _set.size();
    }

    public boolean isEmpty() {
        return _set.isEmpty();
    }

    public boolean contains(Object o) {
        return _set.contains(o);
    }

    public Iterator<E> iterator() {
        return _set.iterator();
    }

    public Object[] toArray() {
        return _set.toArray();
    }

    public <T> T[] toArray(T[] a) {
        return _set.toArray(a);
    }

    public boolean add(E e) {
        return _set.add(e);
    }
    
    /**
     * Does not return a value to avoid a potentially conflicting read. This might improve
     * performance in the context of a transaction.
     */
    public void addOnly(E key) {
    	_set.addOnly(key);
    }

    public boolean remove(Object o) {
        return _set.remove(o);
    }

    public boolean containsAll(Collection<?> c) {
        return _set.containsAll(c);
    }

    public boolean addAll(Collection<? extends E> c) {
        return _set.addAll(c);
    }
    
    /**
     * Does not return a value to avoid a potentially conflicting read. This might improve
     * performance in the context of a transaction.
     */
    public void addAllOnly(Collection<? extends E> c) {
    	_set.addAllOnly(c);
    }

    public boolean retainAll(Collection<?> c) {
        return _set.retainAll(c);
    }

    public boolean removeAll(Collection<?> c) {
        return _set.removeAll(c);
    }
    
    /**
     * Does not return a value to avoid a potentially conflicting read. This might improve
     * performance in the context of a transaction.
     */
    public void removeOnly(Object o) {
    	_set.removeOnly(o);
    }

    public void clear() {
        _set.clear();
    }

	/**
	 * Registers set change listener.
	 * 
	 * @param listener
	 *            {@link SetListener}
	 */
    public void addListener(final SetListener<E> listener) {
        KeyListener<E> resourceListener = new KeyListener<E>() {

            public void onPut(E key) {
                listener.onPut(key);
            }

            public void onRemove(E key) {
                listener.onRemove(key);
            }

            public void onClear() {
                listener.onClear();
            }

        };
        _set.addListener(resourceListener);
        _listeners.put(listener, resourceListener);
    }

	/**
	 * Unregister set change listener previously registered with
	 * {@link SSet#addListener}
	 * 
	 * @param listener
	 *            {@link SetListener}
	 */
    public void removeListener(SetListener<E> listener) {
        _set.removeListener(_listeners.remove(listener));
    }

	/**
	 * Set change listener.
	 * 
	 * @author semenov_pa
	 *
	 */
    public interface SetListener<E> {

    	/**
		 * Fires when the element added to set.
		 */
        public void onPut(E key);

        /**
		 * Fires when the element removed from set.
		 */
        public void onRemove(E key);

        /**
		 * Fires when set cleared.
		 */
        public void onClear();
    }

}
