package com.objectsync.service;

import org.objectfabric.Resource;
import org.objectfabric.TObject;

/**
 * Base abstract class for all synchronized object classes.
 * 
 * @author semenov_pa
 *
 */
class SBase {

	private Resource _resource;

	private TObject _base;

	SBase(Resource resource, TObject object) {
		if (object == null || resource == null) {
			throw new IllegalArgumentException();
		}
		_resource = resource;
		_base = object;
	}

	@Override
	public boolean equals(Object obj) {
		return _base.equals(obj);
	}

	@Override
	public int hashCode() {
		return _base.hashCode();
	}

	@Override
	public String toString() {
		return _base.toString();
	}

	Resource getResource() {
		return _resource;
	}
	
	TObject getBase() {
		return _base;
	}

}
