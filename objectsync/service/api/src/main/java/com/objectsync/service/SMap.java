package com.objectsync.service;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.objectfabric.KeyListener;
import org.objectfabric.Resource;
import org.objectfabric.TMap;

import com.objectsync.service.SArray.ArrayListener;

/**
 * Transactional map. For each thread this class behaves like an HashMap,
 * except: <br>
 * <br>
 * - It does not support null keys. <br>
 * - It does not implement clone. <br>
 * - Entries do not support setValue(V). <br>
 * <br>
 * See comments on {@link SSet} about iterators and concurrency, methods that
 * both read and write, and exceptions.
 */
public final class SMap<K, V> extends SBase implements Map<K, V> {

	private TMap<K, V> _map;

	private Map<MapListener<K>, KeyListener<K>> _listeners = new ConcurrentHashMap<SMap.MapListener<K>, KeyListener<K>>();

	public <B extends SBase> SMap(B base) {
		this(base.getResource(), new TMap<K, V>(base.getResource()));
	}

	SMap(Resource resource, TMap<K, V> map) {
		super(resource, map);
		_map = map;
	}

	public int size() {
		return _map.size();
	}

	public boolean isEmpty() {
		return _map.isEmpty();
	}

	public boolean containsKey(Object key) {
		return _map.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return _map.containsValue(value);
	}

	public V get(Object key) {
		return _map.get(key);
	}

	public V put(K key, V value) {
		return _map.put(key, value);
	}
	
    /**
     * Does not return a value to avoid a potentially conflicting read. This might improve
     * performance in the context of a transaction.
     */
    public void putOnly(K key, V value) {
    	_map.putOnly(key, value);
    }

	public V remove(Object key) {
		return _map.remove(key);
	}
	
    /**
     * Does not return a value to avoid a potentially conflicting read. This might improve
     * performance in the context of a transaction.
     */
    public void removeOnly(Object key) {
    	_map.removeOnly(key);
    }

	public void putAll(Map<? extends K, ? extends V> m) {
		_map.putAll(m);
	}

	public void clear() {
		_map.clear();
	}

	public Set<K> keySet() {
		return _map.keySet();
	}

	public Collection<V> values() {
		return _map.values();
	}

	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return _map.entrySet();
	}

	public void addListener(final MapListener<K> listener) {
		KeyListener<K> mapListener = new KeyListener<K>() {

			public void onPut(K key) {
				listener.onPut(key);
			}

			public void onRemove(K key) {
				listener.onRemove(key);
			}

			public void onClear() {
				listener.onClear();
			}

		};
		_map.addListener(mapListener);
		_listeners.put(listener, mapListener);
	}

	public void removeListener(ArrayListener listener) {
		_map.removeListener(_listeners.remove(listener));
	}

	public interface MapListener<K> {

		public void onPut(K key);

		public void onRemove(K key);

		public void onClear();
	}

}
