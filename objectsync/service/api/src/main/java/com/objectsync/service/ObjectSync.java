package com.objectsync.service;

import java.util.Set;

import org.objectfabric.Address;
import org.objectfabric.AsyncCallback;
import org.objectfabric.Headers;
import org.objectfabric.Resource;
import org.objectfabric.Workspace;

/**
 * Abstract service point class that should be extended by all platform
 * implementations.
 * 
 * @author semenov_pa
 *
 */
abstract class ObjectSync {

	static {
		ObjectSyncObjectModel.register();
	}

	Workspace _workspace;

	String _address;

	String _baseURI;

	String _schema = "ws";

	ObjectSync(Workspace workspace, String address, boolean ssl) {
		_workspace = workspace;
		_address = address;
		_schema = ssl ? "wss" : "ws";
		_baseURI = _schema + "://" + _address;
	}

	Set<String> getAllKeys() {
		// TODO get all keys atomic way
		return null;
	}

	String url(String key) {
		return _baseURI + "/" + key;
	}

	Resource openResource(String key) {
		if (Utils.isEmpty(key)) {
			throw new IllegalArgumentException("Key should not be null");
		}
		Resource resource = _workspace.open(url(key));
		return resource;
	}

	void openResource(String key,
			org.objectfabric.AsyncCallback<Resource> callback) {
		_workspace.openAsync(url(key), callback);
	}

	/**
	 * Opens synchronized object hierarchy synchronous. Caller thread waits until all data
	 * will be loaded.
	 * 
	 * @param key
	 *            location of synchronized object hierarchy
	 * @return {@link SRoot}
	 */
	public SRoot open(String key) {
		Resource resource = openResource(key);
		return new SRoot(resource);
	}

	/**
	 * Opens synchronized object hierarchy asynchronous. Asynchronous result of
	 * the execution is {@link SRoot}
	 * 
	 * @param key
	 *            location of synchronized object hierarchy
	 * @param callback
	 */
	public void open(String key,
			final com.objectsync.service.AsyncCallback<SRoot> callback) {
		openResource(key, new AsyncCallback<Resource>() {

			public void onSuccess(Resource result) {
				callback.onSuccess(new SRoot(result));
			}

			public void onFailure(Exception e) {
				callback.onFailure(e);
			}
		});
	}

	/**
	 * Removes object hierarchy on the <code>key</code> location synchronously. Caller thread waits until
	 * data will be removed.<br>
	 * <br>
	 * Does the same as <br><code>
	 * Resource resource = openResource(key);<br>
	 * resource.set(null);
	 * </code>
	 * 
	 * @param key
	 *            location of synchronized object hierarchy
	 */
	public void removeKey(String key) {
		Resource resource = openResource(key);
		resource.set(null);
	}

	/**
	 * Removes object hierarchy on the <code>key</code> location asynchronously.
	 * 
	 * @param key
	 *            location of synchronized object hierarchy
	 */
	public void removeKey(String key,
			final com.objectsync.service.AsyncCallback<Void> callback) {
		openResource(key, new AsyncCallback<Resource>() {

			public void onSuccess(Resource result) {
				result.set(null);
				callback.onSuccess(null);
			}

			public void onFailure(Exception e) {
				callback.onFailure(e);
			}
		});
	}

    /**
     * Executes the runnable in the context of a transaction, which is a stable snapshot
     * of all workspace objects. If a transaction is already running, the new one is
     * nested. If commit fails due to a conflict with another transaction, all changes are
     * discarded and the runnable is executed again. The process is repeated until the
     * transaction commits or an exception occurs. <br>
     * <br>
     * Consistency and conflict detection are currently not configurable, and set to the
     * following. After executing the runnable, conflicts are detected between reads of
     * the current transaction and writes of other workspace transactions that occurred
     * since the runnable started. If no conflicts are found, the read set is discarded
     * and the write set is propagated to other workspaces and locations in an eventually
     * consistent manner. Writes are applied atomically to each remote resource. No
     * guaranty is made for transactions that span multiple resources.
     */
	public void atomic(Runnable runnable) {
		_workspace.atomic(runnable);
	}
	
    /**
     * Same as atomic, but the transaction is only allowed to read objects. An exception
     * is thrown if invoking an object setter or modifier. This allows higher performance
     * for read-only code, and guarantees the transaction will succeed on first run. For
     * this reason it is safe to modify non-transactional objects or perform operations
     * like writing to the console.
     */
	public void atomicRead(Runnable runnable) {
		_workspace.atomicRead(runnable);
	}
	
    /**
     * Same as atomic, but the transaction does not keep track of its reads. It will
     * successfully commit even if values that were read are in conflict with another
     * transaction's writes. This allows higher performance for code that does not need to
     * check for conflicts, and guarantees the transaction will succeed on first run. For
     * this reason it is safe to modify non-transactional objects or perform operations
     * like writing to the console.
     */
	public void atomicWrite(Runnable runnable) {
		_workspace.atomicWrite(runnable);
	}

	/**
	 * Prevents access to service objects, and unsubscribe from any remote object to allow connections to close.
	 * 
	 * @param callback
	 */
	public void close() {
		_workspace.close();
	}

	/**
	 * Prevents access to service objects, and unsubscribe from any remote object to allow connections to close.
	 * 
	 * @param callback
	 */
	public void close(final AsyncCallback<Void> callback) {
		_workspace.closeAsync(new org.objectfabric.AsyncCallback<Void>() {

			public void onSuccess(Void result) {
				callback.onSuccess(result);
			}

			public void onFailure(Exception e) {
				callback.onFailure(e);
			}

		});
	}

	Headers getHeaders(Address address) {
		// TODO реализовать авторизацию
		return null;
	}
}
