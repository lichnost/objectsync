package com.objectsync.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.objectfabric.Resource;
import org.objectfabric.ResourceListener;
import org.objectfabric.TArray;
import org.objectfabric.TMap;
import org.objectfabric.TObject;
import org.objectfabric.TSet;

/**
 * Root of hierarchy of synchronized object graph.
 * 
 * @author semenov_pa
 *
 */
public final class SRoot extends SBase {

	private Map<RootListener, ResourceListener> _listeners = new ConcurrentHashMap<RootListener, ResourceListener>();

	SRoot(Resource resource) {
		super(resource, resource);
	}

	/**
	 * Gets root object.
	 * 
	 * @return {@link SBase} or null if not set
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public SBase get() {
		TObject object = (TObject) getResource().get();
		SBase result = null;
		if (object instanceof TRef) {
			result = new SObject(getResource(), (TRef) object);
		} else if (object instanceof TArray) {
			result = new SArray(getResource(), (TArray) object);
		} else if (object instanceof TRef) {
			result = new SMap(getResource(), (TMap) object);
		} else if (object instanceof TRef) {
			result = new SSet(getResource(), (TSet) object);
		}
		return result;
	}

	/**
	 * Sets root object of synchronized object graph.
	 * 
	 * @param object that extends {@link SBase}
	 */
	public void set(SBase object) {
		getResource().set(object.getBase());
	}

	@Override
	public boolean equals(Object obj) {
		return getResource().equals(obj);
	}

	@Override
	public int hashCode() {
		return getResource().hashCode();
	}

	@Override
	public String toString() {
		return getResource().toString();
	}

	/**
	 * Registers root object change listener.
	 * 
	 * @param listener {@link RootListener}
	 */
	public void addListener(final RootListener listener) {
		ResourceListener resourceListener = new ResourceListener() {

			public void onSet() {
				listener.onSet();
			}

			public void onDelete() {
				listener.onDelete();
			}
		};
		getResource().addListener(resourceListener);
		_listeners.put(listener, resourceListener);
	}

	/**
	 * Unregister root object change listener previously registered with {@link SRoot#addListener}
	 * 
	 * @param listener {@link RootListener}
	 */
	public void removeListener(RootListener listener) {
		getResource().removeListener(_listeners.remove(listener));
	}

	/**
	 * Root object change listener.
	 * 
	 * @author semenov_pa
	 *
	 */
	public interface RootListener {

		/**
		 * Fires when the new object added to root.
		 */
		public void onSet();

		/**
		 * Fires when the objects removed from root;
		 */
		public void onDelete();

	}

}
