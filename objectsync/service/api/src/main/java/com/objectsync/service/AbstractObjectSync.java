package com.objectsync.service;

import java.util.Set;

import org.objectfabric.Address;
import org.objectfabric.AsyncCallback;
import org.objectfabric.Headers;
import org.objectfabric.Resource;
import org.objectfabric.Workspace;

abstract class AbstractObjectSync {

	static {
		ObjectSyncObjectModel.register();
	}

	Workspace _workspace;

	String _address;

	String _baseURI;

	String _schema = "ws";

	AbstractObjectSync(Workspace workspace, String address, boolean ssl) {
		_workspace = workspace;
		_address = address;
		_schema = ssl ? "wss" : "ws";
		_baseURI = _schema + "://" + _address;
	}

	Set<String> getAllKeys() {
		// TODO get all keys atomic way
		return null;
	}

	String url(String key) {
		return _baseURI + "/" + key;
	}

	Resource openResource(String key) {
		if (Utils.isEmpty(key)) {
			throw new IllegalArgumentException("Key should not be null");
		}
		Resource resource = _workspace.open(url(key));
		return resource;
	}

	void openResource(String key,
			org.objectfabric.AsyncCallback<Resource> callback) {
		_workspace.openAsync(url(key), callback);
	}

	public SRoot open(String key) {
		Resource resource = openResource(key);
		Object obj = resource.get();
		return new SRoot(resource);
	}

	public void open(String key,
			final org.objectfabric.AsyncCallback<SRoot> callback) {
		openResource(key, new AsyncCallback<Resource>() {

			public void onSuccess(Resource result) {
				callback.onSuccess(new SRoot(result));
			}

			public void onFailure(Exception e) {
				callback.onFailure(e);
			}
		});
	}

	public void removeKey(String key) {
		Resource resource = openResource(key);
		resource.set(null);
	}

	public void removeKey(String key, final AsyncCallback<Void> callback) {
		openResource(key, new org.objectfabric.AsyncCallback<Resource>() {

			public void onSuccess(Resource result) {
				result.set(null);
				callback.onSuccess(null);
			}

			public void onFailure(Exception e) {
				callback.onFailure(e);
			}
		});
	}

	public void run(Runnable runnable) {
		_workspace.atomic(runnable);
	}

	public void close() {
		_workspace.close();
	}

	public void close(final AsyncCallback<Void> callback) {
		_workspace.closeAsync(new org.objectfabric.AsyncCallback<Void>() {

			public void onSuccess(Void result) {
				callback.onSuccess(result);
			}

			public void onFailure(Exception e) {
				callback.onFailure(e);
			}

		});
	}

	Headers getHeaders(Address address) {
		// TODO реализовать авторизацию
		return null;
	}
}
