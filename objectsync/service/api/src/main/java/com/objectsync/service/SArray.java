package com.objectsync.service;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.objectfabric.IndexListener;
import org.objectfabric.Resource;
import org.objectfabric.TArray;

/**
 * Transactional array.
 * 
 * @author semenov_pa
 *
 * @param <E>
 *            can be any transactional object that extends or immutable type listed in
 *            {@link ObjectSync}
 */
public final class SArray<E> extends SBase implements Iterable<E> {

	private TArray<E> _array;

	private Map<ArrayListener, IndexListener> _listeners = new ConcurrentHashMap<SArray.ArrayListener, IndexListener>();

	/**
	 * Creates new transactional array.
	 * 
	 * @param base
	 *            parent {@link SBase} object
	 * @param length
	 *            of array
	 */
	public <B extends SBase> SArray(B base, int length) {
		this(base.getResource(), new TArray<E>(base.getResource(), length));
	}

	SArray(Resource resource, TArray<E> array) {
		super(resource, array);
		_array = array;
	}

	public void set(int index, E value) {
		_array.set(index, value);
	}

	public E get(int index) {
		return _array.get(index);
	}

	/**
	 * Length of the array.
	 * 
	 * @return lenght
	 */
	public int length() {
		return _array.length();
	}

	public Iterator<E> iterator() {
		return _array.iterator();
	}

	/**
	 * Registers array change listener.
	 * 
	 * @param listener
	 *            {@link ArrayListener}
	 */
	public void addListener(final ArrayListener listener) {
		IndexListener resourceListener = new IndexListener() {

			public void onSet(int index) {
				listener.onSet(index);
			}

		};
		_array.addListener(resourceListener);
		_listeners.put(listener, resourceListener);
	}

	/**
	 * Unregister array change listener previously registered with
	 * {@link SArray#addListener}
	 * 
	 * @param listener
	 *            {@link ArrayListener}
	 */
	public void removeListener(ArrayListener listener) {
		_array.removeListener(_listeners.remove(listener));
	}

	/**
	 * Array change listener
	 * 
	 * @author semenov_pa
	 *
	 */
	public interface ArrayListener {

		/**
		 * Fires when array element sets.
		 * 
		 * @param index
		 *            of changed element
		 */
		public void onSet(int index);

	}

}
