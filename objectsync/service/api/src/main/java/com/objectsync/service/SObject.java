package com.objectsync.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.objectfabric.IndexListener;
import org.objectfabric.Resource;

import com.objectsync.service.SArray.ArrayListener;

/**
 * Synchronized object holder.
 * 
 * @author semenov_pa
 *
 * @param <E>
 *            can be any synchronized object that extends or immutable type
 *            listed in {@link ObjectSync}
 */
public final class SObject<E> extends SBase {

	private TRef _object;

	private Map<ObjectListener, IndexListener> _listeners = new ConcurrentHashMap<SObject.ObjectListener, IndexListener>();

	public SObject(SBase base) {
		this(base.getResource(), new TRef(base.getResource()));
	}

	SObject(Resource resource, TRef object) {
		super(resource, object);
		_object = object;
	}

	/**
	 * Gets object.
	 * 
	 * @return object or null if not set
	 */
	@SuppressWarnings("unchecked")
	public E get() {
		return (E) _object.object();
	}

	/**
	 * Sets object.
	 * 
	 * @param value
	 */
	public void set(E value) {
		_object.object(value);
	}

	/**
	 * Registers object change listener.
	 * 
	 * @param listener
	 *            {@link ObjectListener}
	 */
	public void addListener(final ObjectListener listener) {
		IndexListener objectListener = new IndexListener() {

			public void onSet(int index) {
				if (index == TRef.OBJECT_INDEX) {
					listener.onSet();
				}
			}
		};
		_object.addListener(objectListener);
		_listeners.put(listener, objectListener);
	}

	/**
	 * Unregister object change listener previously registered with
	 * {@link SObject#addListener}
	 * 
	 * @param listener
	 *            {@link ObjectListener}
	 */
	public void removeListener(ObjectListener listener) {
		_object.removeListener(_listeners.remove(listener));
	}

	/**
	 * Root object change listener.
	 * 
	 * @author semenov_pa
	 *
	 */
	public interface ObjectListener {

		/**
		 * Fires when the new object set.
		 */
		public void onSet();

	}

}
