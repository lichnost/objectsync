/**
 * This file is part of ObjectSync (http://objectsync.com).
 *
 * ObjectSync is licensed under the Apache License, Version 2.0, the terms
 * of which may be found at http://www.apache.org/licenses/LICENSE-2.0.html.
 * 
 * Copyright ObjectSync Inc.
 * 
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

package com.objectsync.generator;

import org.objectfabric.Generator;
import org.objectfabric.ObjectModelDef;

/**
 * This classes invokes ObjectSync code generator to generate an application's
 * object model.
 */
public class GeneratorMain {

	public static void main(String[] args) throws Exception {
		/*
		 * Object models can be imported from XML as in this example, imported
		 * from a relational database (Check SQL sample), or built using code
		 * like below.
		 */
		ObjectModelDef model = ObjectModelDef
				.fromXMLFile("src/main/generator/com/objectsync/generator/ObjectModel.xml");

		/*
		 * Then call the generator, specifying a folder.
		 */
		Generator generator = new Generator(model);
		generator.run("src/main/java");
	}
}
