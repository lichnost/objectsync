# ObjectSync



## Getting Started



```java

...

import com.objectsync.service.ObjectSync;
import com.objectsync.service.JVMObjectSync;
import com.objectsync.service.SRoot;
import com.objectsync.service.SObject;
import com.objectsync.service.SObject.ObjectListener;

...

// Firstly pointing service address of your environment
ObjectSync os = new JVMObjectSync("example.objectsync.com");
SRoot root = os.open("key");
// SRoot is the root of synchronized object hierarchy located by key
// Method ObjectSync.open executed synchronously waiting all resources to load before continue

// Checking is root object exists already
final SObject object = root.get();
if (object == null) {
	// If not creating it and storing in root of hierarchy
	object = new SObject(root);
	root.set(object);
	// This changes synchronize to other connected clients immediately
}
// Subscribe to object change events
object.addListener(new ObjectListener() {

			@Override
            public void onSet() {
				// new object setted
                String value = (String) object.get();
				System.out.println(value);
				// Hello world!
            }

        });
// Now we can set values to our synced object
object.set("Hello world!");

// Close service after using
os.close();
```

```javascript

```
