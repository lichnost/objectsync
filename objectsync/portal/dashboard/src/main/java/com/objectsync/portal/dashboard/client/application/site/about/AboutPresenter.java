package com.objectsync.portal.dashboard.client.application.site.about;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rpc.shared.DispatchAsync;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.objectsync.portal.dashboard.client.application.ApplicationPresenter;
import com.objectsync.portal.dashboard.client.place.NameTokens;

public class AboutPresenter extends
		Presenter<AboutPresenter.MyView, AboutPresenter.MyProxy> implements
		AboutUiHandlers {
	interface MyView extends View, HasUiHandlers<AboutPresenter> {
	}

	@ProxyStandard
	@NameToken(NameTokens.ABOUT)
	@NoGatekeeper
	interface MyProxy extends ProxyPlace<AboutPresenter> {
	}

	PlaceManager _placeManager;
	DispatchAsync _dispatcher;

	@Inject
	AboutPresenter(EventBus eventBus, MyView view, MyProxy proxy,
			PlaceManager placeManager, DispatchAsync dispatcher) {
		super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);

		_placeManager = placeManager;
		_dispatcher = dispatcher;
		getView().setUiHandlers(this);
	}

}
