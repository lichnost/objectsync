package com.objectsync.portal.dashboard.client.application;

import com.gwtplatform.mvp.client.UiHandlers;

public interface ApplicationUiHandlers extends UiHandlers {

	void onAboutNav();

	void onDocumentationNav();

	void onRegisterNav();

	void onLoginNav();

	void onLogoutNav();

}
