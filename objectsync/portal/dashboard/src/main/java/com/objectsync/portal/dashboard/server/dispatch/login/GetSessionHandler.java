package com.objectsync.portal.dashboard.server.dispatch.login;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.google.inject.Provider;
import com.gwtplatform.dispatch.rpc.server.ExecutionContext;
import com.gwtplatform.dispatch.rpc.server.actionhandler.AbstractActionHandler;
import com.gwtplatform.dispatch.shared.ActionException;
import com.objectsync.portal.core.model.User;
import com.objectsync.portal.dashboard.shared.dispatch.login.GetSessionAction;
import com.objectsync.portal.dashboard.shared.dispatch.login.GetSessionResult;

public class GetSessionHandler extends
		AbstractActionHandler<GetSessionAction, GetSessionResult> {

	private Provider<HttpServletRequest> requestProvider;

	@Inject
	public GetSessionHandler(Provider<HttpServletRequest> requestProvider) {
		super(GetSessionAction.class);
		this.requestProvider = requestProvider;
	}

	@Override
	public GetSessionResult execute(GetSessionAction action,
			ExecutionContext context) throws ActionException {
		HttpSession session = requestProvider.get().getSession();
		User user = (User) session.getAttribute(User.class.getName());
		return new GetSessionResult(session.getId(), user);
	}

	@Override
	public void undo(GetSessionAction action, GetSessionResult result,
			ExecutionContext context) throws ActionException {
		throw new ActionException("Undoing not supporeted");
	}

}
