package com.objectsync.portal.dashboard.client;

import javax.inject.Inject;

import com.gwtplatform.mvp.client.annotations.DefaultGatekeeper;
import com.gwtplatform.mvp.client.proxy.Gatekeeper;

@DefaultGatekeeper
public class LoggedInGatekeeper implements Gatekeeper {

	private Session session;

	@Inject
	LoggedInGatekeeper(Session session) {
		this.session = session;
	}

	@Override
	public boolean canReveal() {
		if (session == null) {
			return false;
		}
		return session.isLoggedIn();
	}

}
