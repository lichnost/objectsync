package com.objectsync.portal.dashboard.server.dispatch.workspace;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.google.inject.Provider;
import com.gwtplatform.dispatch.rpc.server.ExecutionContext;
import com.gwtplatform.dispatch.rpc.shared.NoResult;
import com.gwtplatform.dispatch.shared.ActionException;
import com.objectsync.portal.core.PortalCore;
import com.objectsync.portal.dashboard.server.dispatch.SecuredActionHandler;
import com.objectsync.portal.dashboard.shared.dispatch.service.DeleteWorkspaceAction;

public class DeleteWorkspaceHandler extends
		SecuredActionHandler<DeleteWorkspaceAction, NoResult> {

	private PortalCore _portalCore;

	@Inject
	public DeleteWorkspaceHandler(PortalCore portalCore,
			Provider<HttpServletRequest> requestProvider) {
		super(requestProvider, DeleteWorkspaceAction.class);
		_portalCore = portalCore;
	}

	@Override
	public NoResult execute(DeleteWorkspaceAction action, ExecutionContext context)
			throws ActionException {
		checkAuthOrThrow();
		return new NoResult();
	}

	@Override
	public void undo(DeleteWorkspaceAction action, NoResult result,
			ExecutionContext context) throws ActionException {
		throw new ActionException("Undoing not supporeted");
	}

}
