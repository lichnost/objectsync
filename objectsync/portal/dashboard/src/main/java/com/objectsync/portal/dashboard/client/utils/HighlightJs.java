package com.objectsync.portal.dashboard.client.utils;

public class HighlightJs {

	public static native void initHighlightingOnLoad() /*-{
		$wnd.hljs.initHighlightingOnLoad()
	}-*/;

	public static native void initHighlighting() /*-{
		$wnd.hljs.initHighlighting()
	}-*/;

}
