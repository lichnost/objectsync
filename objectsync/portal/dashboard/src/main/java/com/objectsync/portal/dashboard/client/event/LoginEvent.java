package com.objectsync.portal.dashboard.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;

public class LoginEvent extends GwtEvent<LoginEvent.LoginHandler> {

	public interface LoginHandler extends EventHandler {

		void onLogin(LoginEvent event);

	}

	public static final Type<LoginHandler> TYPE = new Type<>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LoginHandler> getAssociatedType() {
		return TYPE;
	}

	public static Type<LoginHandler> getType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LoginHandler handler) {
		handler.onLogin(this);
	}

	public static void fire(HasHandlers source) {
		source.fireEvent(new LoginEvent());
	}

}
