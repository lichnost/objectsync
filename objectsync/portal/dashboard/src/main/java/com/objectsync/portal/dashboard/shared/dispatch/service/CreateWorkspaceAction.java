package com.objectsync.portal.dashboard.shared.dispatch.service;

import com.gwtplatform.dispatch.rpc.shared.NoResult;
import com.gwtplatform.dispatch.rpc.shared.UnsecuredActionImpl;

public class CreateWorkspaceAction extends UnsecuredActionImpl<NoResult> {

	private String workspace;

	public CreateWorkspaceAction() {
	}

	public CreateWorkspaceAction(String workspace) {
		this.workspace = workspace;
	}

	public String getWorkspace() {
		return workspace;
	}

}
