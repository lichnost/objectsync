package com.objectsync.portal.dashboard.client;

import javax.inject.Inject;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rpc.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Bootstrapper;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.objectsync.portal.core.model.User;
import com.objectsync.portal.dashboard.client.event.LoginEvent;
import com.objectsync.portal.dashboard.client.event.LogoutEvent;
import com.objectsync.portal.dashboard.shared.dispatch.login.GetSessionAction;
import com.objectsync.portal.dashboard.shared.dispatch.login.GetSessionResult;

public class ApplicationBootstrapper implements Bootstrapper {

	EventBus _eventBus;
	Session _session;
	PlaceManager _placeManager;
	DispatchAsync _dispatcher;

	@Inject
	public ApplicationBootstrapper(EventBus eventBus, Session session,
			PlaceManager placeManager, DispatchAsync dispatcher) {
		_eventBus = eventBus;
		_session = session;
		_placeManager = placeManager;
		_dispatcher = dispatcher;
	}

	@Override
	public void onBootstrap() {
		_dispatcher.execute(new GetSessionAction(),
				new AsyncCallback<GetSessionResult>() {

					@Override
					public void onFailure(Throwable caught) {
						_eventBus.fireEvent(new LogoutEvent());
						_placeManager.revealCurrentPlace();
					}

					@Override
					public void onSuccess(GetSessionResult result) {
						_session.setId(result.getSessionId());
						User user = result.getUser();
						if (user != null) {
							_session.setLoggedIn(true);
							_session.setUser(user);
							_eventBus.fireEvent(new LoginEvent());
						} else {
							_eventBus.fireEvent(new LogoutEvent());
						}
						_placeManager.revealCurrentPlace();
					}
				});
	}


}
