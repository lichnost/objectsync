package com.objectsync.portal.dashboard.client.application.site.home;

import gwt.material.design.client.ui.MaterialIcon;

import javax.inject.Inject;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class HomeView extends ViewWithUiHandlers<HomePresenter> implements
		HomePresenter.MyView {

	interface Binder extends UiBinder<Widget, HomeView> {
	}

	@UiField
	FocusPanel demoPanelLeft;

	boolean overLeft = false;

	@UiField
	FocusPanel demoPanelRight;

	boolean overRight = false;

	@UiField
	MaterialIcon demoIcon;

	@Inject
	HomeView(Binder uiBinder) {
		initWidget(uiBinder.createAndBindUi(this));
		initDemo();
	}

	private void initDemo() {
		demoPanelLeft.addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent e) {
				overLeft = true;
				demoIcon.setVisibility(Visibility.VISIBLE);
			}
		});
		demoPanelLeft.addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent e) {
				overLeft = false;
				demoIcon.setVisibility(Visibility.HIDDEN);
			}
		});
		demoPanelLeft.addMouseMoveHandler(new MouseMoveHandler() {

			@Override
			public void onMouseMove(MouseMoveEvent e) {
				onLeftMove(e.getX(), e.getY());
			}
		});

		demoPanelRight.addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent e) {
				overRight = true;
				demoIcon.setVisibility(Visibility.VISIBLE);
			}
		});
		demoPanelRight.addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent e) {
				overRight = false;
				demoIcon.setVisibility(Visibility.HIDDEN);
			}
		});
		demoPanelRight.addMouseMoveHandler(new MouseMoveHandler() {

			@Override
			public void onMouseMove(MouseMoveEvent e) {
				onRightMove(e.getX(), e.getY());
			}
		});
	}

	private void onLeftMove(int x, int y) {
		getUiHandlers().onDemoMove(x, y);
	}

	private void onRightMove(int x, int y) {
		getUiHandlers().onDemoMove(x, y);
	}

	@Override
	public void setDemoPositions(int x, int y) {
		if (overRight) {
			setIconPosition(demoPanelLeft.getElement(), demoIcon.getElement(),
					x, y);
		}

		if (overLeft) {
			setIconPosition(demoPanelRight.getElement(), demoIcon.getElement(),
					x, y);
		}
	}

	private void setIconPosition(Element container, Element icon, int pos,
			boolean x) {
		int position = pos;
		if (x) {
			position = container.getAbsoluteLeft() + position;
			position = position + icon.getClientWidth() > container
					.getAbsoluteRight() ? container.getAbsoluteRight()
					- icon.getClientWidth() : position;
			icon.getStyle().setLeft(position, Unit.PX);
		} else {
			position = container.getAbsoluteTop() + position;
			position = position + icon.getClientHeight() > container
					.getAbsoluteBottom() ? container.getAbsoluteBottom()
					- icon.getClientHeight() : position;
			icon.getStyle().setTop(position, Unit.PX);
		}
	}

	private void setIconPosition(Element container, Element icon, int x, int y) {
		setIconPosition(container, icon, x, true);
		setIconPosition(container, icon, y, false);
	}

}
