package com.objectsync.portal.dashboard.client.application.secure.workspace;

import gwt.material.design.client.ui.MaterialToast;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rpc.shared.DispatchAsync;
import com.gwtplatform.dispatch.rpc.shared.NoResult;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.shared.proxy.PlaceRequest;
import com.objectsync.portal.dashboard.client.Session;
import com.objectsync.portal.dashboard.client.application.ApplicationPresenter;
import com.objectsync.portal.dashboard.client.place.NameTokens;
import com.objectsync.portal.dashboard.shared.dispatch.service.CreateWorkspaceAction;

public class CreateWorkspacePresenter extends
		Presenter<CreateWorkspacePresenter.MyView, CreateWorkspacePresenter.MyProxy> implements
		CreateWorkspaceUiHandlers {
	interface MyView extends View, HasUiHandlers<CreateWorkspacePresenter> {
	}

	@ProxyStandard
	@NameToken(NameTokens.WORKSPACE_CREATE)
	interface MyProxy extends ProxyPlace<CreateWorkspacePresenter> {
	}

	Session _session;
	PlaceManager _placeManager;
	DispatchAsync _dispatcher;

	@Inject
	CreateWorkspacePresenter(EventBus eventBus, MyView view, MyProxy proxy,
			Session session, PlaceManager placeManager, DispatchAsync dispatcher) {
		super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);

		_session = session;
		_placeManager = placeManager;
		_dispatcher = dispatcher;
		getView().setUiHandlers(this);
	}

	@Override
	public void onCreate(String workspace) {
		_dispatcher.execute(new CreateWorkspaceAction(workspace), new AsyncCallback<NoResult>() {

			@Override
			public void onFailure(Throwable caught) {
				//TODO error
				MaterialToast.fireToast(caught.getMessage());
			}

			@Override
			public void onSuccess(NoResult result) {
				PlaceRequest req = new PlaceRequest.Builder().nameToken(
						NameTokens.DASHBOARD).build();
				_placeManager.revealPlace(req);
			}
		});
	}

	@Override
	public void onCancel() {
		PlaceRequest req = new PlaceRequest.Builder().nameToken(
				NameTokens.DASHBOARD).build();
		_placeManager.revealPlace(req);
	}
}
