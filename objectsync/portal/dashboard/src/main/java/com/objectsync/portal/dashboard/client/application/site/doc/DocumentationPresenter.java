package com.objectsync.portal.dashboard.client.application.site.doc;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.objectsync.portal.dashboard.client.application.ApplicationPresenter;
import com.objectsync.portal.dashboard.client.place.NameTokens;

public class DocumentationPresenter
		extends
		Presenter<DocumentationPresenter.MyView, DocumentationPresenter.MyProxy>
		implements DocumentationUiHandlers {
	interface MyView extends View, HasUiHandlers<DocumentationPresenter> {

		void setContent(String html);

	}

	@ProxyStandard
	@NameToken(NameTokens.DOCUMENTATION)
	@NoGatekeeper
	interface MyProxy extends ProxyPlace<DocumentationPresenter> {
	}

	PlaceManager _placeManager;

	// DispatchAsync _dispatcher;

	@Inject
	DocumentationPresenter(EventBus eventBus, MyView view, MyProxy proxy,
			PlaceManager placeManager) {
		// , DispatchAsync dispatcher
		super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);

		_placeManager = placeManager;
		// _dispatcher = dispatcher;
		getView().setUiHandlers(this);

		// _dispatcher.execute(new GetDocumentationAction(),
		// new AsyncCallback<SimpleResult<String>>() {
		// @Override
		// public void onFailure(Throwable caught) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void onSuccess(SimpleResult<String> result) {
		// // TODO Auto-generated method stub
		//
		// }
		// });
	}

}
