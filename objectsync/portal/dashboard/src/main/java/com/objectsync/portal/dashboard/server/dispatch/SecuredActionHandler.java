package com.objectsync.portal.dashboard.server.dispatch;

import javax.servlet.http.HttpServletRequest;

import com.google.inject.Provider;
import com.gwtplatform.dispatch.rpc.server.actionhandler.AbstractActionHandler;
import com.gwtplatform.dispatch.rpc.shared.Action;
import com.gwtplatform.dispatch.rpc.shared.Result;
import com.gwtplatform.dispatch.shared.ActionException;
import com.objectsync.portal.core.model.SecuredUser;
import com.objectsync.portal.core.model.User;

public abstract class SecuredActionHandler<A extends Action<R>, R extends Result>
		extends AbstractActionHandler<A, R> {

	private Provider<HttpServletRequest> _requestProvider;

	public SecuredActionHandler(Provider<HttpServletRequest> requestProvider,
			Class<A> actionType) {
		super(actionType);
		_requestProvider = requestProvider;
	}

	protected boolean checkAuth() {
		SecuredUser user = (SecuredUser) _requestProvider.get().getSession()
				.getAttribute(User.class.getName());
		if (user != null) {
			return true;
		}
		return false;
	}

	protected void checkAuthOrThrow() throws ActionException {
		if (!checkAuth()) {
			throw new ActionException("Authorization required");
		}
	}

	protected SecuredUser getUser() {
		return (SecuredUser) _requestProvider.get().getSession()
				.getAttribute(User.class.getName());
	}

}
