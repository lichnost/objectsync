package com.objectsync.portal.dashboard.server.demo;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.objectfabric.JVMWorkspace;
import org.objectfabric.Resource;
import org.objectfabric.TMap;
import org.objectfabric.Workspace;

public class ObjectfabricSessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		Workspace workspace = new JVMWorkspace();
		workspace.addURIHandler(ObjectFabricStarter.store);
		Resource resource = workspace.open("/" + event.getSession().getId()
				+ "-demo-position");
		TMap<String, Integer> position = new TMap<String, Integer>(resource);
		resource.set(position);
		position.putOnly("x", 0);
		position.putOnly("y", 0);
		workspace.close();
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		Workspace workspace = new JVMWorkspace();
		workspace.addURIHandler(ObjectFabricStarter.store);
		Resource resource = workspace.open("/" + event.getSession().getId()
				+ "-demo-position");
		resource.set(null);
		workspace.close();
	}

}
