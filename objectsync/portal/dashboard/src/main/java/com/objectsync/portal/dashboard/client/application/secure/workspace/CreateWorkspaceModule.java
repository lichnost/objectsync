package com.objectsync.portal.dashboard.client.application.secure.workspace;


import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class CreateWorkspaceModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenter(CreateWorkspacePresenter.class, CreateWorkspacePresenter.MyView.class, CreateWorkspaceView.class,
                CreateWorkspacePresenter.MyProxy.class);
    }
}
