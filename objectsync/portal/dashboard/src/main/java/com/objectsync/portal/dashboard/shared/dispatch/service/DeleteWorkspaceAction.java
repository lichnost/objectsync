package com.objectsync.portal.dashboard.shared.dispatch.service;

import com.gwtplatform.dispatch.rpc.shared.NoResult;
import com.gwtplatform.dispatch.rpc.shared.UnsecuredActionImpl;
import com.objectsync.portal.core.model.WorkspaceServer;

public class DeleteWorkspaceAction extends UnsecuredActionImpl<NoResult> {

	private WorkspaceServer workspace;

	public DeleteWorkspaceAction() {
	}

	public DeleteWorkspaceAction(WorkspaceServer workspace) {
		this.workspace = workspace;
	}

	public WorkspaceServer getService() {
		return workspace;
	}

}
