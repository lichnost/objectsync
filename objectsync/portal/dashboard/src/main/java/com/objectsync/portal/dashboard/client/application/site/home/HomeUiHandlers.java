package com.objectsync.portal.dashboard.client.application.site.home;

import com.gwtplatform.mvp.client.UiHandlers;

interface HomeUiHandlers extends UiHandlers {

    void onDemoMove(int x, int y);
	
}
