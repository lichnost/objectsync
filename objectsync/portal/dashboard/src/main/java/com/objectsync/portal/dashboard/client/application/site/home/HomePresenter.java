package com.objectsync.portal.dashboard.client.application.site.home;

import org.objectfabric.AsyncCallback;
import org.objectfabric.GWTWorkspace;
import org.objectfabric.KeyListener;
import org.objectfabric.Resource;
import org.objectfabric.TMap;
import org.objectfabric.WebSocket;
import org.objectfabric.Workspace;

import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rpc.shared.DispatchAsync;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.objectsync.portal.dashboard.client.Session;
import com.objectsync.portal.dashboard.client.application.ApplicationPresenter;
import com.objectsync.portal.dashboard.client.place.NameTokens;

public class HomePresenter extends
		Presenter<HomePresenter.MyView, HomePresenter.MyProxy> implements
		HomeUiHandlers {
	interface MyView extends View, HasUiHandlers<HomePresenter> {

		void setDemoPositions(int x, int y);

	}

	@ProxyStandard
	@NameToken(NameTokens.HOME)
	@NoGatekeeper
	interface MyProxy extends ProxyPlace<HomePresenter> {
	}

	Session session;
	PlaceManager placeManager;
	DispatchAsync dispatcher;
	Workspace workspace;
	Resource resource;
	TMap<String, Integer> map;

	@Inject
	HomePresenter(EventBus eventBus, MyView view, MyProxy proxy,
			Session session, PlaceManager placeManager, DispatchAsync dispatcher) {
		super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);
		this.session = session;
		this.placeManager = placeManager;
		this.dispatcher = dispatcher;
		getView().setUiHandlers(this);
		initWorkspace();
	}

	private void initWorkspace() {
		workspace = new GWTWorkspace();
		workspace.addURIHandler(new WebSocket());
		workspace.openAsync("ws://" + Window.Location.getHostName() + ":8888/" + session.getId()
				+ "-demo-position", new AsyncCallback<Resource>() {

			@Override
			public void onSuccess(Resource result) {
				map = (TMap<String, Integer>) result.get();
				map.addListener(new KeyListener<String>() {

					@Override
					public void onRemove(String key) {
					}

					@Override
					public void onPut(String key) {
						getView().setDemoPositions(map.get("x"), map.get("y"));
					}

					@Override
					public void onClear() {
					}
				});
			}

			@Override
			public void onFailure(Exception e) {

			}
		});

	}

	@Override
	public void onDemoMove(int x, int y) {
		if (x != map.get("x")) {
			map.put("x", x);
		}
		if (y != map.get("y")) {
			map.put("y", y);
		}
	}

}
