package com.objectsync.portal.dashboard.client.application.secure.dashboard;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.objectsync.portal.dashboard.client.application.secure.workspace.CreateWorkspaceModule;

public class DashboardModule extends AbstractPresenterModule {
	@Override
	protected void configure() {
		install(new CreateWorkspaceModule());

		bindPresenter(DashboardPresenter.class,
				DashboardPresenter.MyView.class, DashboardView.class,
				DashboardPresenter.MyProxy.class);
	}
}
