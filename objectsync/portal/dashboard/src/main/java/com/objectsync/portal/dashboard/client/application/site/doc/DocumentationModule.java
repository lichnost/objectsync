package com.objectsync.portal.dashboard.client.application.site.doc;


import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class DocumentationModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenter(DocumentationPresenter.class, DocumentationPresenter.MyView.class, DocumentationView.class,
                DocumentationPresenter.MyProxy.class);
    }
}
