
package com.objectsync.portal.dashboard.client.application.site;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.objectsync.portal.dashboard.client.application.site.about.AboutModule;
import com.objectsync.portal.dashboard.client.application.site.doc.DocumentationModule;
import com.objectsync.portal.dashboard.client.application.site.home.HomeModule;
import com.objectsync.portal.dashboard.client.application.site.login.LoginModule;
import com.objectsync.portal.dashboard.client.application.site.register.RegisterModule;

public class SiteModule extends AbstractPresenterModule {

    @Override
    protected void configure() {
        install(new HomeModule());
        install(new AboutModule());
        install(new DocumentationModule());
        install(new LoginModule());
        install(new RegisterModule());
    }
}
