package com.objectsync.portal.dashboard.client.application.secure.dashboard;

import com.gwtplatform.mvp.client.UiHandlers;
import com.objectsync.portal.core.model.WorkspaceServer;

public interface DashboardUiHandlers extends UiHandlers {

	void onCreate();

	void onDelete(WorkspaceServer service);

}
