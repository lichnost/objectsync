package com.objectsync.portal.dashboard.server.dispatch.workspace;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.google.inject.Provider;
import com.gwtplatform.dispatch.rpc.server.ExecutionContext;
import com.gwtplatform.dispatch.rpc.shared.NoResult;
import com.gwtplatform.dispatch.shared.ActionException;
import com.objectsync.portal.core.PortalCore;
import com.objectsync.portal.core.model.WorkspaceServer;
import com.objectsync.portal.dashboard.server.dispatch.SecuredActionHandler;
import com.objectsync.portal.dashboard.shared.dispatch.service.CreateWorkspaceAction;

public class CreateWorkspaceHandler extends
		SecuredActionHandler<CreateWorkspaceAction, NoResult> {

	private PortalCore _portalCore;

	@Inject
	public CreateWorkspaceHandler(PortalCore portalCore,
			Provider<HttpServletRequest> requestProvider) {
		super(requestProvider, CreateWorkspaceAction.class);
		_portalCore = portalCore;
	}

	@Override
	public NoResult execute(CreateWorkspaceAction action,
			ExecutionContext context) throws ActionException {
		checkAuthOrThrow();
		if (_portalCore.isWorspaceAvaliable(action.getWorkspace())) {
			throw new ActionException("Workspace name not available");
		}
		WorkspaceServer server = new WorkspaceServer();
		server.setStarted(true);
		server.setWorkspace(action.getWorkspace());
		server.setUser(getUser());
		_portalCore.saveWorkspaceServer(server);
		return new NoResult();
	}

	@Override
	public void undo(CreateWorkspaceAction action, NoResult result,
			ExecutionContext context) throws ActionException {
		throw new ActionException("Undoing not supporeted");
	}

}
