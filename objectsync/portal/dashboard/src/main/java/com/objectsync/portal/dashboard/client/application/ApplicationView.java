package com.objectsync.portal.dashboard.client.application;

import gwt.material.design.client.ui.MaterialLink;
import gwt.material.design.client.ui.MaterialNavBar;
import gwt.material.design.client.ui.MaterialNavSection;

import javax.inject.Inject;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class ApplicationView extends ViewWithUiHandlers<ApplicationPresenter>
		implements ApplicationPresenter.MyView {

	interface Binder extends UiBinder<Widget, ApplicationView> {
	}

	@UiField
	MaterialNavBar navBar;

	@UiField
	HTMLPanel mainContainer;

	@UiField
	MaterialNavSection navSection;

	@UiField
	MaterialLink aboutLink;
	boolean aboutLinkVisible = true;

	@UiField
	MaterialLink documentationLink;
	boolean documentationLinkVisible = true;

	@UiField
	MaterialLink registerLink;
	boolean registerLinkVisible = true;

	@UiField
	MaterialLink loginLink;
	boolean loginLinkVisible = true;

	@UiField
	MaterialLink logoutLink;
	boolean logoutLinkVisible = true;

	@Inject
	ApplicationView(Binder uiBinder) {
		initWidget(uiBinder.createAndBindUi(this));

		bindSlot(ApplicationPresenter.SLOT_MAIN, mainContainer);
	}

	@Override
	public void setNavLoginVisible(boolean visible) {
		loginLinkVisible = visible;
		rebuildNav();
	}

	@Override
	public void setNavRegisterVisible(boolean visible) {
		registerLinkVisible = visible;
		rebuildNav();
	}

	@Override
	public void setNavLogoutVisible(boolean visible) {
		logoutLinkVisible = visible;
		rebuildNav();
	}

	void rebuildNav() {
		navSection.clear();

		navSection.add(aboutLink);
		navSection.add(documentationLink);
		if (registerLinkVisible) {
			navSection.add(registerLink);
		}
		if (loginLinkVisible) {
			navSection.add(loginLink);
		}
		if (logoutLinkVisible) {
			navSection.add(logoutLink);
		}
	}

	@UiHandler("aboutLink")
	void onAbout(ClickEvent event) {
		getUiHandlers().onAboutNav();
	}

	@UiHandler("documentationLink")
	void onDocumentation(ClickEvent event) {
		getUiHandlers().onDocumentationNav();
	}

	@UiHandler("registerLink")
	void onRegister(ClickEvent event) {
		getUiHandlers().onRegisterNav();
	}
	
	@UiHandler("loginLink")
	void onLogin(ClickEvent event) {
		getUiHandlers().onLoginNav();
	}

	@UiHandler("logoutLink")
	void onLogout(ClickEvent event) {
		getUiHandlers().onLogoutNav();
	}
}
