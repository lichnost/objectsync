package com.objectsync.portal.dashboard.server.dispatch;

import com.gwtplatform.dispatch.rpc.server.guice.HandlerModule;
import com.objectsync.portal.dashboard.server.dispatch.doc.GetDocumentationHandler;
import com.objectsync.portal.dashboard.server.dispatch.login.GetSessionHandler;
import com.objectsync.portal.dashboard.server.dispatch.login.LoginHandler;
import com.objectsync.portal.dashboard.server.dispatch.register.RegisterHandler;
import com.objectsync.portal.dashboard.server.dispatch.workspace.CreateWorkspaceHandler;
import com.objectsync.portal.dashboard.server.dispatch.workspace.DeleteWorkspaceHandler;
import com.objectsync.portal.dashboard.server.dispatch.workspace.GetUserWorkspaceHandler;
import com.objectsync.portal.dashboard.shared.dispatch.doc.GetDocumentationAction;
import com.objectsync.portal.dashboard.shared.dispatch.login.GetSessionAction;
import com.objectsync.portal.dashboard.shared.dispatch.login.LoginAction;
import com.objectsync.portal.dashboard.shared.dispatch.register.RegisterAction;
import com.objectsync.portal.dashboard.shared.dispatch.service.CreateWorkspaceAction;
import com.objectsync.portal.dashboard.shared.dispatch.service.DeleteWorkspaceAction;
import com.objectsync.portal.dashboard.shared.dispatch.service.GetUserWorkspaceAction;

public class ServerModule extends HandlerModule {

	@Override
	protected void configureHandlers() {
		bindHandler(GetSessionAction.class, GetSessionHandler.class);
		bindHandler(LoginAction.class, LoginHandler.class);
		bindHandler(RegisterAction.class, RegisterHandler.class);
		bindHandler(GetDocumentationAction.class, GetDocumentationHandler.class);
		bindHandler(GetUserWorkspaceAction.class, GetUserWorkspaceHandler.class);
		bindHandler(CreateWorkspaceAction.class, CreateWorkspaceHandler.class);
		bindHandler(DeleteWorkspaceAction.class, DeleteWorkspaceHandler.class);
	}

}
