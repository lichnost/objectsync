package com.objectsync.portal.dashboard.client.application.secure.dashboard;

import gwt.material.design.client.ui.MaterialToast;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rpc.shared.DispatchAsync;
import com.gwtplatform.dispatch.rpc.shared.NoResult;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.shared.proxy.PlaceRequest;
import com.objectsync.portal.core.model.WorkspaceServer;
import com.objectsync.portal.dashboard.client.Session;
import com.objectsync.portal.dashboard.client.application.ApplicationPresenter;
import com.objectsync.portal.dashboard.client.place.NameTokens;
import com.objectsync.portal.dashboard.shared.dispatch.service.DeleteWorkspaceAction;

public class DashboardPresenter extends
		Presenter<DashboardPresenter.MyView, DashboardPresenter.MyProxy> implements
		DashboardUiHandlers {
	interface MyView extends View, HasUiHandlers<DashboardPresenter> {
	}

	@ProxyStandard
	@NameToken(NameTokens.DASHBOARD)
	interface MyProxy extends ProxyPlace<DashboardPresenter> {
	}

	Session _session;
	PlaceManager _placeManager;
	DispatchAsync _dispatcher;

	@Inject
	DashboardPresenter(EventBus eventBus, MyView view, MyProxy proxy,
			Session session, PlaceManager placeManager, DispatchAsync dispatcher) {
		super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);

		_session = session;
		_placeManager = placeManager;
		_dispatcher = dispatcher;
		getView().setUiHandlers(this);
	}
	
	

	@Override
	public void onCreate() {
		PlaceRequest req = new PlaceRequest.Builder().nameToken(
				NameTokens.WORKSPACE_CREATE).build();
		_placeManager.revealPlace(req);
	}

	@Override
	public void onDelete(WorkspaceServer workspace) {
		_dispatcher.execute(new DeleteWorkspaceAction(workspace), new AsyncCallback<NoResult>() {

			@Override
			public void onFailure(Throwable caught) {
				//TODO error
				MaterialToast.fireToast(caught.getMessage());
			}

			@Override
			public void onSuccess(NoResult result) {
				
			}
		});
	}
}
