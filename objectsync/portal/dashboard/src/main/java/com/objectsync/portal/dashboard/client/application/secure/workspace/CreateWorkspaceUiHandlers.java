package com.objectsync.portal.dashboard.client.application.secure.workspace;

import com.gwtplatform.mvp.client.UiHandlers;

interface CreateWorkspaceUiHandlers extends UiHandlers {

	void onCreate(String workspace);

	void onCancel();
	
}
