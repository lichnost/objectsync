package com.objectsync.portal.dashboard.server.demo;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.codec.http.HttpRequestDecoder;
import org.jboss.netty.handler.codec.http.HttpResponseEncoder;
import org.objectfabric.JVMServer;
import org.objectfabric.Memory;
import org.objectfabric.NettySession;
import org.objectfabric.Server;
import org.objectfabric.URIHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObjectFabricStarter implements ServletContextListener {

	private static Logger _log = LoggerFactory
			.getLogger(ObjectFabricStarter.class);

	static URIHandler store;
	private static Channel channel;

	@Override
	public void contextInitialized(ServletContextEvent event) {
		store = new Memory(false);

		final Server server = new JVMServer();
		server.addURIHandler(store);

		ServerBootstrap bootstrap = new ServerBootstrap(
				new NioServerSocketChannelFactory(
						Executors.newCachedThreadPool(),
						Executors.newCachedThreadPool()));

		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {

			@Override
			public ChannelPipeline getPipeline() throws Exception {
				ChannelPipeline pipeline = Channels.pipeline();
				pipeline.addLast("decoder", new HttpRequestDecoder());
				pipeline.addLast("encoder", new HttpResponseEncoder());
				pipeline.addLast("objectfabric", new NettySession(server));
				return pipeline;
			}
		});

		channel = bootstrap.bind(new InetSocketAddress(8888));
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		try {
			channel.unbind().await();
		} catch (InterruptedException e) {
			_log.warn(e.getMessage(), e);
		}
	}
}
