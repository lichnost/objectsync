package com.objectsync.portal.dashboard.shared.dispatch.login;

import com.gwtplatform.dispatch.rpc.shared.Result;
import com.objectsync.portal.core.model.User;

public class GetSessionResult implements Result {

	private String sessionId;
	private User user;

	public GetSessionResult() {
	}

	public GetSessionResult(String sessionId, User user) {
		this.sessionId = sessionId;
		this.user = user;
	}

	public String getSessionId() {
		return sessionId;
	}
	
	public User getUser() {
		return user;
	}

}
