package com.objectsync.portal.dashboard.client.application.secure.workspace;

import gwt.material.design.client.ui.MaterialTextBox;

import javax.inject.Inject;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class CreateWorkspaceView extends ViewWithUiHandlers<CreateWorkspacePresenter> implements
		CreateWorkspacePresenter.MyView {
	interface Binder extends UiBinder<Widget, CreateWorkspaceView> {
	}

	@UiField
	MaterialTextBox workspace;

	@Inject
	CreateWorkspaceView(Binder uiBinder) {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("createButton")
	void onLogin(ClickEvent event) {
		getUiHandlers().onCreate(workspace.getValue());
	}
	
	@UiHandler("cancelButton")
	void onRegister(ClickEvent event) {
		getUiHandlers().onCancel();
	}

}
