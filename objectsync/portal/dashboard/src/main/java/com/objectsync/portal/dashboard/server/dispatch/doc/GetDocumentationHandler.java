package com.objectsync.portal.dashboard.server.dispatch.doc;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwtplatform.dispatch.rpc.server.ExecutionContext;
import com.gwtplatform.dispatch.rpc.server.actionhandler.AbstractActionHandler;
import com.gwtplatform.dispatch.rpc.shared.SimpleResult;
import com.gwtplatform.dispatch.shared.ActionException;
import com.objectsync.portal.dashboard.shared.dispatch.doc.GetDocumentationAction;

public class GetDocumentationHandler extends
		AbstractActionHandler<GetDocumentationAction, SimpleResult<String>> {

	private static Logger _log = LoggerFactory
			.getLogger(GetDocumentationHandler.class);

	private static String _documentation;

	public GetDocumentationHandler() {
		super(GetDocumentationAction.class);
	}

	@Override
	public SimpleResult<String> execute(GetDocumentationAction action,
			ExecutionContext context) throws ActionException {
		if (_documentation == null) {
			try (DataInputStream md = new DataInputStream(
					new BufferedInputStream(
							GetDocumentationHandler.class
									.getResourceAsStream("documentation.md")))) {
				_documentation = md.readUTF();
			} catch (IOException e) {
				_log.warn(e.getMessage(), e);
			}
		}
		return new SimpleResult<String>(_documentation);
	}

	@Override
	public void undo(GetDocumentationAction action,
			SimpleResult<String> result, ExecutionContext context)
			throws ActionException {
		throw new ActionException("Undoing not supporeted");
	}

}
