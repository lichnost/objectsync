package com.objectsync.portal.dashboard.client.application;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.objectsync.portal.dashboard.client.application.secure.dashboard.DashboardModule;
import com.objectsync.portal.dashboard.client.application.site.SiteModule;

public class ApplicationModule extends AbstractPresenterModule {
	@Override
	protected void configure() {
		install(new SiteModule());
		install(new DashboardModule());
		
		bindPresenter(ApplicationPresenter.class,
				ApplicationPresenter.MyView.class, ApplicationView.class,
				ApplicationPresenter.MyProxy.class);


	}
}
