package com.objectsync.portal.dashboard.client.resources;

import javax.inject.Inject;

import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.resources.client.TextResource;

public class ResourceLoader {
	@Inject
	ResourceLoader(AppResources appResources) {
		appResources.normalize().ensureInjected();
		appResources.style().ensureInjected();

		injectJs(appResources.highlightjs());
	}

	private void injectJs(TextResource resource) {
		String text = resource.getText() + "//# sourceURL="
				+ resource.getName() + ".js";

		// Inject the script resource
		ScriptInjector.fromString(text).setRemoveTag(false)
				.setWindow(ScriptInjector.TOP_WINDOW).inject();
	}

}
