package com.objectsync.portal.dashboard.server.dispatch.workspace;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.google.inject.Provider;
import com.gwtplatform.dispatch.rpc.server.ExecutionContext;
import com.gwtplatform.dispatch.rpc.shared.MultipleResult;
import com.gwtplatform.dispatch.shared.ActionException;
import com.objectsync.portal.core.PortalCore;
import com.objectsync.portal.core.model.WorkspaceServer;
import com.objectsync.portal.dashboard.server.dispatch.SecuredActionHandler;
import com.objectsync.portal.dashboard.shared.dispatch.service.GetUserWorkspaceAction;

public class GetUserWorkspaceHandler
		extends
		SecuredActionHandler<GetUserWorkspaceAction, MultipleResult<WorkspaceServer>> {

	private PortalCore _portalCore;

	@Inject
	public GetUserWorkspaceHandler(PortalCore portalCore,
			Provider<HttpServletRequest> requestProvider) {
		super(requestProvider, GetUserWorkspaceAction.class);
		_portalCore = portalCore;
	}

	@Override
	public MultipleResult<WorkspaceServer> execute(GetUserWorkspaceAction action,
			ExecutionContext context) throws ActionException {
		checkAuthOrThrow();		
		return new MultipleResult<WorkspaceServer>(_portalCore.getUserWorkspaceServers(getUser()));
	}

	@Override
	public void undo(GetUserWorkspaceAction action,
			MultipleResult<WorkspaceServer> result, ExecutionContext context)
			throws ActionException {
		throw new ActionException("Undoing not supporeted");
	}

}
