package com.objectsync.portal.dashboard.client;

import com.objectsync.portal.core.model.User;

public class Session {

	private String id;
	private boolean loggedIn = false;
	private User user;

	public Session() {

	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	public void setUser(User _user) {
		this.user = _user;
	}

	public User getUser() {
		return user;
	}
	
}
