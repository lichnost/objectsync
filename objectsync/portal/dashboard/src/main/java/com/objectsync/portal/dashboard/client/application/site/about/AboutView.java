package com.objectsync.portal.dashboard.client.application.site.about;

import javax.inject.Inject;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class AboutView extends ViewWithUiHandlers<AboutPresenter> implements
		AboutPresenter.MyView {
	interface Binder extends UiBinder<Widget, AboutView> {
	}

	@Inject
	AboutView(Binder uiBinder) {
		initWidget(uiBinder.createAndBindUi(this));
	}


}
