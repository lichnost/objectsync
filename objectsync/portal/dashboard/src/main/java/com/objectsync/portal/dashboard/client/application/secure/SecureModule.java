package com.objectsync.portal.dashboard.client.application.secure;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.objectsync.portal.dashboard.client.application.secure.dashboard.DashboardModule;
import com.objectsync.portal.dashboard.client.application.secure.workspace.CreateWorkspaceModule;

public class SecureModule extends AbstractPresenterModule {
	@Override
	protected void configure() {
		install(new DashboardModule());
		install(new CreateWorkspaceModule());
	}
}
