package com.objectsync.portal.dashboard.client.application.site.doc;

import javax.inject.Inject;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import com.objectsync.portal.dashboard.client.utils.HighlightJs;

public class DocumentationView extends
		ViewWithUiHandlers<DocumentationPresenter> implements
		DocumentationPresenter.MyView {
	interface Binder extends UiBinder<Widget, DocumentationView> {
	}


	@UiField
	HTML htmlPanel;

	@Inject
	DocumentationView(Binder uiBinder) {
		initWidget(uiBinder.createAndBindUi(this));
	}



	@Override
	public void setContent(String html) {
		htmlPanel.setHTML(html);
		HighlightJs.initHighlighting();
	}

}
