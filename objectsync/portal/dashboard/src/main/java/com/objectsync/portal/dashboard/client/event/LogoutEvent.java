package com.objectsync.portal.dashboard.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;

public class LogoutEvent extends GwtEvent<LogoutEvent.LogoutHandler> {

	public interface LogoutHandler extends EventHandler {

		void onLogout(LogoutEvent event);

	}

	public static final Type<LogoutHandler> TYPE = new Type<>();

	public LogoutEvent() {
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LogoutHandler> getAssociatedType() {
		return TYPE;
	}
	
	public static Type<LogoutHandler> getType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LogoutHandler handler) {
		handler.onLogout(this);
	}

	public static void fire(HasHandlers source) {
		source.fireEvent(new LogoutEvent());
	}

}
