package com.objectsync.portal.dashboard.client.application;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyEvent;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.presenter.slots.NestedSlot;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.shared.proxy.PlaceRequest;
import com.objectsync.portal.dashboard.client.event.LoginEvent;
import com.objectsync.portal.dashboard.client.event.LoginEvent.LoginHandler;
import com.objectsync.portal.dashboard.client.event.LogoutEvent;
import com.objectsync.portal.dashboard.client.event.LogoutEvent.LogoutHandler;
import com.objectsync.portal.dashboard.client.place.NameTokens;

public class ApplicationPresenter extends
		Presenter<ApplicationPresenter.MyView, ApplicationPresenter.MyProxy>
		implements LoginHandler, LogoutHandler, ApplicationUiHandlers {

	interface MyView extends View, HasUiHandlers<ApplicationPresenter> {

		void setNavLoginVisible(boolean visible);

		void setNavLogoutVisible(boolean visible);

		void setNavRegisterVisible(boolean visible);

	}

	@ProxyStandard
	@NoGatekeeper
	interface MyProxy extends Proxy<ApplicationPresenter> {
	}

	public static final NestedSlot SLOT_MAIN = new NestedSlot();

	PlaceManager placeManager;

	@Inject
	ApplicationPresenter(EventBus eventBus, MyView view, MyProxy proxy,
			PlaceManager placeManager) {
		super(eventBus, view, proxy, RevealType.Root);
		this.placeManager = placeManager;

		getView().setUiHandlers(this);
	}

	@ProxyEvent
	@Override
	public void onLogin(LoginEvent event) {
		getView().setNavLoginVisible(false);
		getView().setNavRegisterVisible(false);
		getView().setNavLogoutVisible(true);
	}

	@ProxyEvent
	@Override
	public void onLogout(LogoutEvent event) {
		getView().setNavLoginVisible(true);
		getView().setNavRegisterVisible(true);
		getView().setNavLogoutVisible(false);
	}

	@Override
	public void onAboutNav() {
		PlaceRequest req = new PlaceRequest.Builder().nameToken(
				NameTokens.ABOUT).build();
		placeManager.revealPlace(req);
	}

	@Override
	public void onDocumentationNav() {
		PlaceRequest req = new PlaceRequest.Builder().nameToken(
				NameTokens.DOCUMENTATION).build();
		placeManager.revealPlace(req);
	}

	@Override
	public void onRegisterNav() {
		PlaceRequest req = new PlaceRequest.Builder().nameToken(
				NameTokens.REGISTER).build();
		placeManager.revealPlace(req);
	}

	@Override
	public void onLoginNav() {
		PlaceRequest req = new PlaceRequest.Builder().nameToken(
				NameTokens.LOGIN).build();
		placeManager.revealPlace(req);
	}

	@Override
	public void onLogoutNav() {
//		PlaceRequest req = new PlaceRequest.Builder()
//				.nameToken(NameTokens.HOME).build();
//		_placeManager.revealPlace(req);
	}

}
