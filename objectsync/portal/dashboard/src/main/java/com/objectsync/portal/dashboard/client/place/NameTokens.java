package com.objectsync.portal.dashboard.client.place;

public class NameTokens {
	public static final String HOME = "!home";
	public static final String ABOUT = "!about";
	public static final String DASHBOARD = "!dashboard";
	public static final String LOGIN = "!login";
	public static final String REGISTER = "!register";
	public static final String WORKSPACE_CREATE = "!workspace-create";
	public static final String DOCUMENTATION = "!documentation";
}
