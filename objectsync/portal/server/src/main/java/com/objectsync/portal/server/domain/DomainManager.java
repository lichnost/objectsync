package com.objectsync.portal.server.domain;

import com.objectsync.portal.core.model.WorkspaceServer;

public interface DomainManager {

	void register(WorkspaceServer server, int port);

	void unregister(WorkspaceServer server);

}
