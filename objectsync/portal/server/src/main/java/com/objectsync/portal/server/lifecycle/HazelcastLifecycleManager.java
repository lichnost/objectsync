package com.objectsync.portal.server.lifecycle;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import com.hazelcast.core.IQueue;
import com.hazelcast.core.MemberAttributeEvent;
import com.hazelcast.core.MembershipEvent;
import com.hazelcast.core.MembershipListener;
import com.hazelcast.core.MultiMap;
import com.objectsync.portal.core.PortalCore;
import com.objectsync.portal.core.model.WorkspaceServer;

public class HazelcastLifecycleManager implements MembershipListener,
		LifecycleManager, AutoCloseable {

	private static String START_WORKSPACE_QUEUE = "workspace-start-queue";
	private static String STOP_WORKSPACE_QUEUE = "workspace-stop-queue";
	private static String STARTED_WORKSPACES_MAP = "workspaces-started-map";
	private static String LOCK = "lock";
	private static String REMOVE_LOCK = "lock";

	private PortalCore _portal;
	private HazelcastLifecycleSettings _settings;
	private WorkspaceHandler _handler;

	private HazelcastInstance _hazelcast;
	private IQueue<WorkspaceServer> _startQueue;
	private IQueue<WorkspaceServer> _stopQueue;
	private MultiMap<String, WorkspaceServer> _startedMap;

	private Thread _startThread = new Thread() {

		@Override
		public void run() {
			while (true) {
				try {
					WorkspaceServer server = _startQueue.take();
					_lock.lock();
					try {
						if (_handler != null
								&& _handler.onStart(server) == com.objectsync.portal.server.lifecycle.LifecycleManager.State.STARTED) {
							_startedMap.put(_hazelcast.getCluster()
									.getLocalMember().getUuid(), server);
						} else {
							_startQueue.put(server);
						}
					} finally {
						_lock.unlock();
					}
				} catch (InterruptedException e) {
					break;
				}
			}
		}

	};
	private Thread _stopThread = new Thread() {
		@Override
		public void run() {
			while (true) {
				try {
					WorkspaceServer server = _stopQueue.take();
					_lock.lock();
					try {
						if (_handler != null
								&& _handler.onStop(server) == com.objectsync.portal.server.lifecycle.LifecycleManager.State.STOPED) {
							String memberId = _hazelcast.getCluster()
									.getLocalMember().getUuid();
							WorkspaceServer v = contains(
									_startedMap.get(memberId), server);
							_startedMap.remove(memberId, v);
						} else {
							_stopQueue.put(server);
						}
					} finally {
						_lock.unlock();
					}
				} catch (InterruptedException e) {
					break;
				}
			}
		};

	};

	private ILock _lock;
	private ILock _removeLock;

	@Inject
	public HazelcastLifecycleManager(PortalCore portal,
			HazelcastLifecycleSettings settings) {
		_portal = portal;
		_settings = settings;

		Config config = new Config();
		config.getGroupConfig().setName(_settings.getGroupName());
		config.getGroupConfig().setPassword(_settings.getGroupPassword());

		_hazelcast = Hazelcast.newHazelcastInstance(config);
		_hazelcast.getCluster().addMembershipListener(this);

		_startQueue = _hazelcast.getQueue(START_WORKSPACE_QUEUE);
		_stopQueue = _hazelcast.getQueue(STOP_WORKSPACE_QUEUE);
		_startedMap = _hazelcast.getMultiMap(STARTED_WORKSPACES_MAP);
		_lock = _hazelcast.getLock(LOCK);
		_removeLock = _hazelcast.getLock(REMOVE_LOCK);

		_startThread.start();
		_stopThread.start();
	}

	@Override
	public void memberAdded(MembershipEvent membershipEvent) {

	}

	@Override
	public void memberRemoved(MembershipEvent membershipEvent) {
		if (_removeLock.tryLock()) {
			try {
				_lock.lock();
				try {
					String memberId = membershipEvent.getMember().getUuid();
					if (_startedMap.containsKey(memberId)) {
						_startedMap.remove(memberId);
					}
				} finally {
					_lock.unlock();
				}
				checkStarts();
			} finally {
				_removeLock.unlock();
			}
		}
	}

	@Override
	public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
	}

	/**
	 * Check new servers to start.
	 */
	@Override
	public void checkStarts() {
		_lock.lock();
		try {
			List<WorkspaceServer> servers = _portal.getWorkspaceServers();
			for (WorkspaceServer s : servers) {
				if (s.isStarted()) {
					if (contains(_startedMap.values(), s) == null
							&& contains(_startQueue.iterator(), s) == null) {
						_startQueue.add(s);
					}
				}
			}
		} finally {
			_lock.unlock();
		}
	}

	private WorkspaceServer contains(Iterator<WorkspaceServer> iter,
			WorkspaceServer server) {
		while (iter.hasNext()) {
			WorkspaceServer v = iter.next();
			if (server.equals(v)) {
				return v;
			}
		}
		return null;
	}

	private WorkspaceServer contains(Collection<WorkspaceServer> all,
			WorkspaceServer server) {
		return contains(all.iterator(), server);
	}

	@Override
	public void checkStops() {
		_lock.lock();
		try {
			List<WorkspaceServer> servers = _portal.getWorkspaceServers();
			for (WorkspaceServer s : servers) {
				if (!s.isStarted()) {
					WorkspaceServer v = contains(_startedMap.values(), s);
					if (v != null && contains(_stopQueue.iterator(), s) == null) {
						_stopQueue.add(v);
					}
				}
			}
		} finally {
			_lock.unlock();
		}
	}

	@Override
	public int startedCount() {
		return _startedMap.size();
	}

	public int startedCountLocal() {
		return _startedMap.valueCount(_hazelcast.getCluster().getLocalMember()
				.getUuid());
	}

	@Override
	public void setWorkspaceHandler(WorkspaceHandler handler) {
		_handler = handler;
	}

	@Override
	public void close() {
		_startThread.interrupt();
		_stopThread.interrupt();
		_hazelcast.shutdown();
	}

}
