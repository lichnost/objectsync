package com.objectsync.portal.server.domain;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

public class AddressUtil {

	public static String getLocalAddress() {
		String localAddress;
		try {
			Enumeration<NetworkInterface> all = NetworkInterface
					.getNetworkInterfaces();
			while (all.hasMoreElements()) {
				NetworkInterface i = all.nextElement();
				if (!i.isLoopback() && !i.isPointToPoint() && i.isUp()) {
					Enumeration<InetAddress> addresses = i.getInetAddresses();
					while (addresses.hasMoreElements()) {
						InetAddress a = addresses.nextElement();
						localAddress = a.getHostAddress();
						return localAddress;
					}
				}
			}
		} catch (SocketException e) {
			try {
				localAddress = InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e1) {
				localAddress = InetAddress.getLoopbackAddress()
						.getHostAddress();
			}
			return localAddress;
		}
		throw new RuntimeException("Address resolving failed");
	}

}
