package com.objectsync.portal.server.domain;

import javax.inject.Inject;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import com.lambdaworks.redis.RedisConnectionPool;
import com.lambdaworks.redis.RedisException;
import com.lambdaworks.redis.RedisURI;
import com.objectsync.portal.core.Util;
import com.objectsync.portal.core.model.WorkspaceServer;

public class HipacheDomainManager implements DomainManager {

	private HipacheDomainSettings _settings;
	private RedisClient _redis;
	private RedisConnectionPool<RedisConnection<String, String>> _pool;

	@Inject
	public HipacheDomainManager(HipacheDomainSettings settings) {
		_settings = settings;
		StringBuilder url = new StringBuilder();
		url.append("redis://");
		if (!Util.isEmpty(_settings.getHipacheRedisPassword())) {
			url.append(_settings.getHipacheRedisPassword() + "@");
		}
		url.append(_settings.getHipacheRedisHost() + ":");
		url.append(_settings.getHipacheRedisPort());
		_redis = RedisClient.create(RedisURI.create(url.toString()));
		_pool = _redis.pool();
	}

	private boolean isRegistered(WorkspaceServer server) {
		try (RedisConnection<String, String> connection = connection()) {
			String key = frontendKey(server.getWorkspace());
			return connection.exists(key);
		}
	}

	@Override
	public void register(WorkspaceServer server, int port) {
		try (RedisConnection<String, String> connection = connection()) {
			if (isRegistered(server)) {
				unregister(server);
			}
			String key = frontendKey(server.getWorkspace());
			connection.rpush(key, server.getWorkspace());
			connection.rpush(key, "ws://" + _settings.getLocalAddress() + ":"
					+ port);
		}
	}

	private String frontendKey(String subdomain) {
		return "frontend:" + subdomain + "." + _settings.getDomain();
	}

	@Override
	public void unregister(WorkspaceServer server) {
		try (RedisConnection<String, String> connection = connection()) {
			String key = frontendKey(server.getWorkspace());
			connection.del(key);
		} catch (RedisException e) {
			throw new RuntimeException(e);
		}
	}

	private RedisConnection<String, String> connection() {
		return _pool.allocateConnection();
	}

}
