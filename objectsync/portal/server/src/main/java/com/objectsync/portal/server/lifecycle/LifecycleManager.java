package com.objectsync.portal.server.lifecycle;

import com.objectsync.portal.core.model.WorkspaceServer;

public interface LifecycleManager {

	public enum State {
		STARTED, STOPED, CONTINUE, REJECT 
	}

	public interface WorkspaceHandler {

		public State onStart(WorkspaceServer server);

		public State onStop(WorkspaceServer server);

	}

	void setWorkspaceHandler(WorkspaceHandler handler);

	void checkStarts();

	void checkStops();

	int startedCount();

}
