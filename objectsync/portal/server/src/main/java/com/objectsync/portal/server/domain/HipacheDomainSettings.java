package com.objectsync.portal.server.domain;

public interface HipacheDomainSettings extends DomainSettings {

	String getHipacheRedisHost();

	String getHipacheRedisPassword();

	int getHipacheRedisPort();

}
