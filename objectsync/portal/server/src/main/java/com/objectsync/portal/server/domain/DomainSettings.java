package com.objectsync.portal.server.domain;

public interface DomainSettings {

	String getLocalAddress();

	String getDomain();

}
