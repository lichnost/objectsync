package com.objectsync.portal.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.cassandra.exceptions.ConfigurationException;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import redis.embedded.RedisServer;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import com.lambdaworks.redis.RedisURI;
import com.objectsync.portal.core.CoreException;
import com.objectsync.portal.core.PortalCore;
import com.objectsync.portal.core.model.SecuredUser;
import com.objectsync.portal.core.model.User;
import com.objectsync.portal.core.model.WorkspaceServer;
import com.objectsync.portal.server.domain.AddressUtil;
import com.objectsync.portal.server.domain.DomainManager;
import com.objectsync.portal.server.domain.HipacheDomainManager;
import com.objectsync.portal.server.domain.HipacheDomainSettings;
import com.objectsync.portal.server.lifecycle.HazelcastLifecycleManager;
import com.objectsync.portal.server.lifecycle.HazelcastLifecycleSettings;
import com.objectsync.portal.server.lifecycle.LifecycleManager;
import com.objectsync.portal.server.lifecycle.LifecycleManager.State;
import com.objectsync.portal.server.lifecycle.LifecycleManager.WorkspaceHandler;
import com.objectsync.portal.server.workspace.CassandraWorkspaceManager;
import com.objectsync.portal.server.workspace.CassandraWorkspaceSettings;

public class ServerTest {

	int workspacesCount = 10;
	boolean allWorkspacesStarted = true;

	class CountDownLatchHandler implements WorkspaceHandler {

		CountDownLatch startLatch;
		CountDownLatch stopLatch;

		CountDownLatchHandler(CountDownLatch start, CountDownLatch stop) {
			startLatch = start;
			stopLatch = stop;
		}

		@Override
		public State onStart(WorkspaceServer server) {
			System.out.println("Works server started: " + server.getId() + ":"
					+ server.getWorkspace());
			startLatch.countDown();
			return State.STARTED;
		}

		@Override
		public State onStop(WorkspaceServer server) {
			System.out.println("Works server stopped: " + server.getId() + ":"
					+ server.getWorkspace());
			stopLatch.countDown();
			return State.STOPED;
		}

	}

	@Before
	public void before() {

	}

	// lifecycle test
	private LifecycleManager lifecycle() {
		PortalCore portal = new PortalCore() {

			@Override
			public void saveWorkspaceServer(WorkspaceServer server) {

			}

			@Override
			public SecuredUser register(String email, String password)
					throws CoreException {
				return null;
			}

			@Override
			public SecuredUser login(String email, String password) {
				return null;
			}

			@Override
			public List<WorkspaceServer> getWorkspaceServers() {
				List<WorkspaceServer> result = new ArrayList<WorkspaceServer>();

				for (int i = 0; i < workspacesCount; i++) {
					WorkspaceServer server = new WorkspaceServer();
					server.setId(String.valueOf(i));
					server.setWorkspace("test" + i);
					server.setStarted(allWorkspacesStarted);
					result.add(server);
				}

				// trying to start/stop second instance
				WorkspaceServer server = new WorkspaceServer();
				server.setId(String.valueOf(workspacesCount - 1));
				server.setWorkspace("test" + (workspacesCount - 1));
				server.setStarted(allWorkspacesStarted);
				result.add(server);

				return result;
			}

			@Override
			public List<WorkspaceServer> getUserWorkspaceServers(User user) {
				return null;
			}

			@Override
			public boolean isWorspaceAvaliable(String workspace)
					throws CoreException {
				return true;
			}
		};
		HazelcastLifecycleSettings settings = new HazelcastLifecycleSettings() {

			@Override
			public String getGroupPassword() {
				return "test-group-password";
			}

			@Override
			public String getGroupName() {
				return "test-group";
			}
		};
		HazelcastLifecycleManager lifecycle = new HazelcastLifecycleManager(
				portal, settings);
		return lifecycle;
	}

	@Test
	public void test_lifecycle() {

		// test single instance
		CountDownLatch startLatch = new CountDownLatch(workspacesCount);
		CountDownLatch stopLatch = new CountDownLatch(workspacesCount);

		LifecycleManager lifecycleInstance1 = lifecycle();

		lifecycleInstance1.setWorkspaceHandler(new CountDownLatchHandler(
				startLatch, stopLatch));
		// start all
		allWorkspacesStarted = true;
		lifecycleInstance1.checkStarts();
		try {
			startLatch.await(workspacesCount * 10, TimeUnit.SECONDS);
			Thread.sleep(workspacesCount * 300);
		} catch (InterruptedException e) {
		}
		Assert.assertEquals(workspacesCount, lifecycleInstance1.startedCount());

		// stop all
		allWorkspacesStarted = false;
		lifecycleInstance1.checkStops();
		try {
			stopLatch.await(workspacesCount * 10, TimeUnit.SECONDS);
			Thread.sleep(workspacesCount * 300);
		} catch (InterruptedException e) {
		}
		Assert.assertEquals(0, lifecycleInstance1.startedCount());

		// test multiply instances
		startLatch = new CountDownLatch(workspacesCount);
		stopLatch = new CountDownLatch(workspacesCount);
		lifecycleInstance1.setWorkspaceHandler(new CountDownLatchHandler(
				startLatch, stopLatch));

		LifecycleManager lifecycleInstance2 = lifecycle();
		lifecycleInstance2.setWorkspaceHandler(new CountDownLatchHandler(
				startLatch, stopLatch));

		LifecycleManager lifecycleInstance3 = lifecycle();
		lifecycleInstance3.setWorkspaceHandler(new CountDownLatchHandler(
				startLatch, stopLatch));

		// start all
		allWorkspacesStarted = true;
		lifecycleInstance1.checkStarts();
		try {
			startLatch.await(workspacesCount * 10, TimeUnit.SECONDS);
			Thread.sleep(workspacesCount * 300);
		} catch (InterruptedException e) {
		}
		Assert.assertEquals(workspacesCount, lifecycleInstance1.startedCount());

		// stop all
		allWorkspacesStarted = false;
		lifecycleInstance1.checkStops();
		try {
			stopLatch.await(workspacesCount * 10, TimeUnit.SECONDS);
			Thread.sleep(workspacesCount * 300);
		} catch (InterruptedException e) {
		}
		Assert.assertEquals(0, lifecycleInstance1.startedCount());

		close(lifecycleInstance1);
		close(lifecycleInstance2);
		close(lifecycleInstance3);

	}

	private void close(Object object) {
		if (object instanceof AutoCloseable) {
			try {
				((AutoCloseable) object).close();
			} catch (Exception e) {
			}
		}
	}

	// workspace test
	class WorkspaceTestDomainManager implements DomainManager {

		Map<WorkspaceServer, Integer> servers = new HashMap<WorkspaceServer, Integer>();
		boolean portCollision = false;

		@Override
		public void register(WorkspaceServer server, int port) {
			if (servers.containsValue(port)) {
				portCollision = true;
			}
			servers.put(server, port);
		}

		@Override
		public void unregister(WorkspaceServer server) {
			servers.remove(server);
		}

	}

	@Test
	public void test_workspace() throws ConfigurationException,
			TTransportException, IOException, InterruptedException {
		EmbeddedCassandraServerHelper.startEmbeddedCassandra(60000);
		CassandraWorkspaceSettings settings = new CassandraWorkspaceSettings() {

			@Override
			public String getUsername() {
				return null;
			}

			@Override
			public int getPort() {
				return EmbeddedCassandraServerHelper.getNativeTransportPort();
			}

			@Override
			public String getPassword() {
				return null;
			}

			@Override
			public String getHost() {
				return EmbeddedCassandraServerHelper.getHost();
			}
		};
		LifecycleManager lifecycle = new LifecycleManager() {

			@Override
			public void setWorkspaceHandler(WorkspaceHandler handler) {
			}

			@Override
			public void checkStarts() {
			}

			@Override
			public void checkStops() {
			}

			@Override
			public int startedCount() {
				return 0;
			}

		};
		WorkspaceTestDomainManager domain = new WorkspaceTestDomainManager();
		CassandraWorkspaceManager manager = new CassandraWorkspaceManager(
				settings, lifecycle, domain);

		// start all
		for (int i = 0; i < workspacesCount; i++) {
			WorkspaceServer server = new WorkspaceServer();
			server.setId(String.valueOf(i));
			server.setWorkspace("test" + i);
			Assert.assertEquals(State.STARTED, manager.onStart(server));

		}
		Assert.assertEquals(workspacesCount, domain.servers.size());
		Assert.assertFalse(domain.portCollision);

		// trying start already started workspace
		WorkspaceServer serverDouble = new WorkspaceServer();
		serverDouble.setId(String.valueOf(workspacesCount - 1));
		serverDouble.setWorkspace("test" + (workspacesCount - 1));
		Assert.assertEquals(State.REJECT, manager.onStart(serverDouble));

		// stop all
		for (int i = 0; i < workspacesCount; i++) {
			WorkspaceServer server = new WorkspaceServer();
			server.setId(String.valueOf(i));
			server.setWorkspace("test" + i);
			Assert.assertEquals(State.STOPED, manager.onStop(server));
		}
		Assert.assertEquals(0, domain.servers.size());
	}

	@Test
	public void test_domain() throws Exception {
		final RedisServer redisServer = RedisServer.builder().port(6379).setting("maxheap 128M")
				.build();
		redisServer.start();
		try {
			HipacheDomainSettings settings = new HipacheDomainSettings() {

				@Override
				public String getLocalAddress() {
					return AddressUtil.getLocalAddress();
				}

				@Override
				public String getDomain() {
					return "example";
				}

				@Override
				public int getHipacheRedisPort() {
					return redisServer.ports().iterator().next();
				}

				@Override
				public String getHipacheRedisPassword() {
					return null;
				}

				@Override
				public String getHipacheRedisHost() {
					return "localhost";
				}
			};

			RedisClient redisClient = RedisClient.create(RedisURI
					.create("redis://" + settings.getHipacheRedisHost() + ":"
							+ settings.getHipacheRedisPort()));
			RedisConnection<String, String> redisConnection = redisClient
					.connect();

			HipacheDomainManager domain = new HipacheDomainManager(settings);

			for (int i = 0; i < workspacesCount; i++) {
				WorkspaceServer server = new WorkspaceServer();
				server.setId(String.valueOf(i));
				server.setWorkspace("test" + i);
				domain.register(server, 1024 + i);
			}
			Assert.assertEquals(workspacesCount,
					redisConnection.keys("frontend:test*").size());

			// register double server
			WorkspaceServer serverDouble = new WorkspaceServer();
			serverDouble.setId(String.valueOf(workspacesCount - 1));
			serverDouble.setWorkspace("test" + (workspacesCount - 1));
			domain.register(serverDouble, 1024 + workspacesCount - 1);
			Assert.assertEquals(workspacesCount,
					redisConnection.keys("frontend:test*").size());

			for (int i = 0; i < workspacesCount; i++) {
				WorkspaceServer server = new WorkspaceServer();
				server.setId(String.valueOf(i));
				server.setWorkspace("test" + i);
				domain.unregister(server);
			}
			Assert.assertEquals(0, redisConnection.keys("frontend:test*")
					.size());

			redisClient.shutdown();
		} finally {
			redisServer.stop();
		}

	}

	@After
	public void after() {

	}

}
