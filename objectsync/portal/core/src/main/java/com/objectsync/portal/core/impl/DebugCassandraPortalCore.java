package com.objectsync.portal.core.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.cassandra.exceptions.ConfigurationException;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Cluster.Builder;
import com.datastax.driver.core.Session;

public class DebugCassandraPortalCore extends CassandraPortalCore {

	private static CassandraPortalSettings SETTINGS = new CassandraPortalSettings() {

		@Override
		public String getUsername() {
			return null;
		}

		@Override
		public int getPort() {
			return EmbeddedCassandraServerHelper.getNativeTransportPort();
		}

		@Override
		public String getPassword() {
			return null;
		}

		@Override
		public String getMainKeyspace() {
			return "objectsync_service";
		}

		@Override
		public String getHost() {
			return EmbeddedCassandraServerHelper.getHost();
		}
	};

	static {
		try {
			EmbeddedCassandraServerHelper.startEmbeddedCassandra(60000);

			Builder builder = Cluster.builder().addContactPoint(
					EmbeddedCassandraServerHelper.getHost());
			builder.withPort(EmbeddedCassandraServerHelper
					.getNativeTransportPort());
			Cluster cluster = builder.build();
			Session session = cluster.connect();

			BufferedReader r = new BufferedReader(new InputStreamReader(
					CassandraPortalCore.class.getResourceAsStream("init.sql")));
			String l;
			while ((l = r.readLine()) != null) {
				if (!l.trim().isEmpty()) {
					session.execute(l);
					if (l.contains("KEYSPACE")) {
						session.execute("USE " + SETTINGS.getMainKeyspace()
								+ ";");
					}
				}
			}
			r.close();

			session.close();
			cluster.close();

			System.out.println("=========================");
			System.out.println("Debug Cassandra batabase started");
			System.out.println("Host: "
					+ EmbeddedCassandraServerHelper.getHost());
			System.out.println("Port: "
					+ EmbeddedCassandraServerHelper.getNativeTransportPort());
			System.out.println("=========================");
		} catch (ConfigurationException | TTransportException | IOException
				| InterruptedException e) {
		}
	}

	public DebugCassandraPortalCore() throws IOException {
		super(SETTINGS);

	}
}
