package com.objectsync.portal.core;

import java.util.List;

import com.objectsync.portal.core.model.SecuredUser;
import com.objectsync.portal.core.model.User;
import com.objectsync.portal.core.model.WorkspaceServer;

public interface PortalCore {

	SecuredUser register(String email, String password) throws CoreException;

	SecuredUser login(String email, String password) throws CoreException;

	List<WorkspaceServer> getWorkspaceServers() throws CoreException;

	List<WorkspaceServer> getUserWorkspaceServers(User user)
			throws CoreException;

	boolean isWorspaceAvaliable(String workspace) throws CoreException;

	void saveWorkspaceServer(WorkspaceServer server) throws CoreException;

}
